{
  description = "My Nix flake";

  inputs = {
    # Channels
    nixpkgs.url = "github:nixos/nixpkgs/nixos-24.11";
    unstable.url = "github:nixos/nixpkgs/nixos-unstable";

    snowfall-lib.url = "github:snowfallorg/lib";
    snowfall-lib.inputs.nixpkgs.follows = "nixpkgs";

    darwin.url = "github:lnl7/nix-darwin/nix-darwin-24.11";
    darwin.inputs.nixpkgs.follows = "nixpkgs";

    home-manager.url = "github:nix-community/home-manager/release-24.11";
    home-manager.inputs.nixpkgs.follows = "nixpkgs";

    agenix.url = "github:ryantm/agenix";
    agenix.inputs.nixpkgs.follows = "nixpkgs";

    nix-index-database.url = "github:nix-community/nix-index-database";
    nix-index-database.inputs.nixpkgs.follows = "nixpkgs";

    devenv.url = "github:cachix/devenv";
    devenv.inputs.nixpkgs.follows = "nixpkgs";

    disko.url = "github:nix-community/disko";
    disko.inputs.nixpkgs.follows = "nixpkgs";

    vscode-server.url = "github:nix-community/nixos-vscode-server";
    vscode-server.inputs.nixpkgs.follows = "nixpkgs";

    nvidia-patch.url = "github:icewind1991/nvidia-patch-nixos";
    nvidia-patch.inputs.nixpkgs.follows = "nixpkgs";

    stylix.url = "github:danth/stylix/release-24.11";
    stylix.inputs.nixpkgs.follows = "nixpkgs";

    nixos-facter-modules.url = "github:numtide/nixos-facter-modules";
    impermanence.url = "github:nix-community/impermanence";
  };

  outputs = inputs:
    inputs.snowfall-lib.mkFlake {
      inherit inputs;
      src = ./.;

      channels-config = {
        allowUnfree = true;
        allowUnsupportedSystem = true;
        permittedInsecurePackages = [ ];
      };

      snowfall = {
        root = ./.;
        namespace = "nover";
        meta = {
          name = "nover-machines-config";
          title = "Nover's Machines configuration";
        };
      };

      overlays = with inputs; [
        agenix.overlays.default
        nvidia-patch.overlays.default
      ];

      systems.modules.nixos = with inputs; [
        agenix.nixosModules.default
        disko.nixosModules.default
        home-manager.nixosModules.home-manager
        nix-index-database.nixosModules.nix-index
        stylix.nixosModules.stylix
        vscode-server.nixosModule
      ];

      systems.modules.darwin = with inputs;  [
        agenix.darwinModules.default
        home-manager.darwinModules.home-manager
        nix-index-database.darwinModules.nix-index
        stylix.darwinModules.stylix
      ];

      homes.modules = with inputs; [
        nix-index-database.hmModules.nix-index
      ];
    };
}
