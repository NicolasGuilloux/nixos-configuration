{ pkgs, ... }:

{
  programs.btop.enable = true;
  programs.btop.package = pkgs.btop.override { cudaSupport = true; };
}
