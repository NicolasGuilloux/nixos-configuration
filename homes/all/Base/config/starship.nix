{ lib, host, ... }:

let
  colors = {
    directory = "red";
    git = "green";
    status = "purple";
    versions = "grey";
    time = "blue";
  };

  formatColors = input:
    if lib.strings.isStringLike input then
      formatColors { bg = input; }
    else
      builtins.concatStringsSep " " [
        (if (builtins.hasAttr "fg" input) then "fg:${input.fg}" else "")
        (if (builtins.hasAttr "bg" input) then "bg:${input.bg}" else "")
      ];
in
{
  # Starship
  programs.starship.enable = true;
  programs.starship.enableTransience = true;

  programs.starship.settings = {
    # Left part
    format = builtins.concatStringsSep "" [
      "[](${formatColors { fg = colors.directory; }})"
      "$directory"
      "[](${formatColors { fg = colors.directory; bg = colors.git; }})"
      "$git_branch"
      "$git_status"
      "[ ](${formatColors { fg = colors.git; }})"
    ];

    # Right part
    right_format = builtins.concatStringsSep "" [
      "[](${formatColors { fg = colors.status; }})"
      "$status"
      "$os"
      "$username"
      "[${host} ](${formatColors { bg = colors.status; }})"
      "[](${formatColors { fg = colors.status; bg = colors.versions; }})"
      "$c"
      "$elixir"
      "$elm"
      "$golang"
      "$haskell"
      "$julia"
      "$nodejs"
      "$nim"
      "$nix_shell"
      "$php"
      "$rust"
      "$docker"
      "[](${formatColors { fg = colors.versions; bg = colors.time; }})"
      "$time"
      "[ ](${formatColors { fg = colors.time; }})"
    ];

    add_newline = true;

    username = {
      show_always = false;
      style_user = formatColors colors.status;
      style_root = formatColors colors.status;
      format = "[$user ]($style)";
    };

    directory = {
      style = formatColors colors.directory;
      format = "[ $path ]($style)";
      truncation_length = 3;
      truncate_to_repo = false;
      truncation_symbol = "…/";
    };

    c = {
      symbol = "";
      style = formatColors colors.versions;
      format = "[ $symbol ($version) ]($style)";
    };

    docker_context = {
      symbol = "";
      style = formatColors colors.versions;
      format = "[ $symbol $context ]($style)";
    };

    elixir = {
      symbol = "";
      style = formatColors colors.versions;
      format = "[ $symbol ($version) ]($style)";
    };

    elm = {
      symbol = "";
      style = formatColors colors.versions;
      format = "[ $symbol ($version) ]($style)";
    };

    git_branch = {
      symbol = "";
      style = formatColors colors.git;
      format = "[ $symbol $branch ]($style)";
    };

    git_status = {
      style = formatColors colors.git;
      diverged = "🖖 ";
      stashed = "📦 ";
      # modified = "✏️ ";
      format = "[$all_status$ahead_behind]($style)";
    };

    golang = {
      symbol = "";
      style = formatColors colors.versions;
      format = "[ $symbol ($version) ]($style)";
    };

    haskell = {
      symbol = "";
      style = formatColors colors.versions;
      format = "[ $symbol ($version) ]($style)";
    };

    nodejs = {
      symbol = "";
      style = formatColors colors.versions;
      format = "[ $symbol ($version) ]($style)";
    };

    nix_shell = {
      symbol = "";
      style = formatColors colors.versions;
      format = "[ $symbol ($version) ]($style)";
    };

    php = {
      symbol = "🐘";
      style = formatColors colors.versions;
      format = "[ $symbol ($version) ]($style)";
    };

    rust = {
      symbol = "";
      style = formatColors colors.versions;
      format = "[ $symbol ($version) ]($style)";
    };

    status = {
      disabled = false;
      style = formatColors colors.status;
      format = "[$symbol$status ]($style)";
    };

    shell = {
      disabled = false;
      style = formatColors colors.status;
      format = "[$indicator ]($style)";
      bash_indicator = " ";
      fish_indicator = "🐠 ";
      zsh_indicator = "zsh ";
    };

    time = {
      disabled = false;
      time_format = "%R";
      style = formatColors colors.time;
      format = "[ $time ]($style)";
    };

    os = {
      disabled = false;
      style = formatColors colors.status;
      format = "[$indicator ]($style)";
      symbols = {
        Windows = "󰍲";
        Ubuntu = "󰕈";
        SUSE = "";
        Raspbian = "󰐿";
        Mint = "󰣭";
        Macos = "󰀵";
        Manjaro = "";
        Linux = "󰌽";
        Gentoo = "󰣨";
        Fedora = "󰣛";
        Alpine = "";
        Amazon = "";
        Android = "";
        Arch = "󰣇";
        Artix = "󰣇";
        EndeavourOS = "";
        CentOS = "";
        Debian = "󰣚";
        Redhat = "󱄛";
        RedHatEnterprise = "󱄛";
        NixOS = "";
      };
    };
  };
}
