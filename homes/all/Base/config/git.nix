{ ... }:

{
  # Default config
  programs.git = {
    userName = "Nicolas Guilloux";
    userEmail = "nicolas.guilloux@protonmail.com";
  };
}
