{ ... }:

{
  programs.alacritty = {
    enable = true;
    settings = {
      # Misc
      env.TERM = "xterm-256color";

      # Window
      window = {
        dynamic_title = true;
        dynamic_padding = true;
        decorations = "full";
        # opacity = 0.9;
        padding = {
          x = 10;
          y = 10;
        };
      };

      # Scrolling
      scrolling = {
        history = 10000;
        multiplier = 3;
      };

      # Mouse
      mouse.hide_when_typing = true;

      # Cursor
      cursor.style = "Block";
      cursor.unfocused_hollow = true;

      # Hints<
      hints.enabled = [
        #   {
        #     regex = "(magnet:|mailto:|gemini:|gopher:|https:|http:|news:|file:|git:|ssh:|ftp:)[^\\u0000-\\u001F\\u007F-\\u009F<>\\\"\\\\\\s{-}\\\\^⟨⟩`]+";
        #     command = "open";
        #     post_processing = true;
        #     mouse = {
        #       enabled = true;
        #       mods = "Command";
        #     };
        #     binding = {
        #       key = "U";
        #       mods = "Control|Shift";
        #     };
        #   }
        {
          command = "open";
          hyperlinks = true;
          mouse = {
            enabled = true;
            mods = "Command";
          };
          binding = {
            key = "U";
            mods = "Control|Shift";
          };
        }
      ];
    };
  };
}
