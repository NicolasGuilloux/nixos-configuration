{ lib, ... }:

{
  imports = [ ../../all/Base ]
    ++ (lib.snowfall.fs.get-non-default-nix-files-recursive ./.);

  home.stateVersion = "23.11";
  home.sessionPath = [ "$HOME/.local/bin" ];
}

