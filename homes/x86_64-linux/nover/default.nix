{ lib, ... }:

{
  imports = [ ../../all/Base ]
    ++ (lib.snowfall.fs.get-non-default-nix-files-recursive ./.);
}
