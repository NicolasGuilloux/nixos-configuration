{ ... }:

_final: prev:

{
  virt-manager = prev.virt-manager.overrideAttrs (_oldAttrs: {
    postInstallPhase = ''
      wrapProgram $out/bin/virt-manager --add-flags "--no-fork"
    '';
  });
}
