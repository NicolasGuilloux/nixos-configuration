{ config, lib, pkgs, ... }:

let
  cfg = config.services.home-assistant-remote;
in
{
  config = lib.mkIf cfg.enable {
    environment.systemPackages = builtins.map
      (name: pkgs.writeScriptBin "virtual-machine-${name}" ''
          case $1 in

          start)
            virsh start ${name}
            ;;

          stop)
            virsh destroy ${name}
            ;;

          status)
            virsh domstate ${name}
            ;;

          *)
            echo "Unsupported command: $1"
            ;;
        esac
      '')
      cfg.virtualMachines;

    services.home-assistant-remote.allowedSudoCommands = lib.concatLists (
      builtins.map
        (name: [
          "/usr/bin/env virtual-machine-${name} start"
          "/usr/bin/env virtual-machine-${name} stop"
          "/usr/bin/env virtual-machine-${name} status"
        ])
        cfg.virtualMachines
    );
  };
}
