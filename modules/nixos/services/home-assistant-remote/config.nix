{ lib, ... }:

with lib;
with types;

{
  options.services.home-assistant-remote = {
    # services.home-assistant-remote.enable
    enable = mkEnableOption "Enabled Home Assistant remote control";

    # services.home-assistant-remote.user
    user = mkOption {
      type = str;
      description = "User used for remote control";
      default = "home-assistant";
    };

    # services.home-assistant-remote.allowedCommands
    authorizedSshKeys = mkOption {
      type = listOf str;
      description = "Public SSH keys that are authorized to access the user";
      default = [ ];
    };

    # services.home-assistant-remote.allowedCommands
    allowedSudoCommands = mkOption {
      type = listOf str;
      description = "Allowed commands that can be run under sudo";
      default = [ ];
    };

    # services.home-assistant-remote.docker-compose-controllers.*
    docker-compose-controllers = mkOption {
      type = attrsOf str;
      default = { };
      example = { project = "/home/user/project/docker-compose.yaml"; };
      description = "Path as string to the docker-compose file";
    };

    # services.home-assistant-remote.virtualMachines.*
    virtualMachines = mkOption {
      type = listOf str;
      default = [ ];
      description = "List of VM to control using libvirt";
    };
  };
}
