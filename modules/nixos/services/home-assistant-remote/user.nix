{ config, lib, pkgs, ... }:

let
  cfg = config.services.home-assistant-remote;
in
{
  users.users."${cfg.user}" = lib.mkIf cfg.enable {
    createHome = lib.mkDefault true;
    home = "/var/lib/home-assistant-remote";
    description = lib.mkDefault "Home Assistant remote user";
    group = lib.mkDefault "nogroup";
    extraGroups = lib.mkDefault [ ];
    isSystemUser = lib.mkDefault true;
    shell = lib.mkDefault pkgs.bashInteractive;
    openssh.authorizedKeys.keys = cfg.authorizedSshKeys;
  };
}
