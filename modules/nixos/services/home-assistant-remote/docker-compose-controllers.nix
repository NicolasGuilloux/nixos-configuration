{ config, lib, pkgs, ... }:

let
  cfg = config.services.home-assistant-remote;
in
{
  config = lib.mkIf cfg.enable {
    environment.systemPackages = lib.mapAttrsToList
      (name: path: pkgs.writeScriptBin "docker-compose-controller-${name}" ''
          case $1 in

          start)
            docker-compose -f "${path}" up -d
            ;;

          stop)
            docker-compose -f "${path}" down --remove-orphans
            ;;

          status)
            if [ ! -z "$(docker-compose -f "${path}" ps --quiet)" ]
            then
              echo 0
            fi
            ;;

          *)
            echo "Unsupported command: $1"
            ;;
        esac
      '')
      cfg.docker-compose-controllers;

    services.home-assistant-remote.allowedSudoCommands = lib.concatLists (
      lib.mapAttrsToList
        (name: _path: [
          "/usr/bin/env docker-compose-controller-${name} start"
          "/usr/bin/env docker-compose-controller-${name} stop"
          "/usr/bin/env docker-compose-controller-${name} status"
        ])
        cfg.docker-compose-controllers
    );
  };
}
