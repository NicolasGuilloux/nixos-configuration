{ config, lib, ... }:

let
  cfg = config.services.home-assistant-remote;
in
{
  security.sudo.extraConfig = lib.mkAfter (
    if cfg.enable then
      "${cfg.user} ALL=(ALL) NOPASSWD: ${builtins.concatStringsSep ", " cfg.allowedSudoCommands}"
    else
      ""
  );
}
