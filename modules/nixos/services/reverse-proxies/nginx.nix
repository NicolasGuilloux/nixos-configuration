{ config, lib, ... }:

let
  proxies = lib.filterAttrs (_key: value: value.enable) config.services.reverseProxies.services;
in
{
  services.nginx.virtualHosts =
    lib.mapAttrs'
      (
        name: value:
          lib.nameValuePair ("${name}.${value.domain}") {
            forceSSL = true;
            enableACME = true;
            extraConfig = "proxy_buffering off";

            locations."/" = {
              proxyPass = "${value.url}:${toString value.port}";
              proxyWebsockets = true;
              extraConfig =
                # required when the target is also TLS server with multiple hosts
                "proxy_ssl_server_name on;" +
                # required when the server wants to use HTTP Authentication
                "proxy_pass_header Authorization;";
            };
          }
      )
      proxies;
}
