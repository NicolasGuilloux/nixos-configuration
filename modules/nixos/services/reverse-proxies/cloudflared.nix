{ config, lib, ... }:

let
  cfg = config.services.cloudflared;
  reverseProxiesConfig = lib.filterAttrs (_n: v: v.provider == "cloudflare") config.services.reverseProxies.services;
  regularProxies = lib.filterAttrs (_key: value: value.enable) reverseProxiesConfig;
in
{
  services.cloudflared.tunnels."${config.networking.hostName}" = {
    warp-routing.enabled = true;

    ingress = lib.mapAttrs'
      (name: value: {
        name = "${name}.${value.domain}";
        value = {
          service = "https://127.0.0.1";
          originRequest = {
            noTLSVerify = true;
            originServerName = "*.nicolasguilloux.eu";
            connectTimeout = "30s";
          };
        };
      })
      regularProxies;
  };

  systemd.services = lib.mapAttrs'
    (name: value: {
      name = "cloudflared-${name}-dns-record";
      value = {
        enable = true;
        description = "Create the DNS entry \"${name}\" to point to this server";
        wantedBy = [ "cloudflared-tunnel-${config.networking.hostName}.service" ];
        script = ''
          ${cfg.package}/bin/cloudflared tunnel route dns ${config.networking.hostName} ${name} || exit 0 
        '';
        serviceConfig = {
          User = "cloudflared";
        };
      };
    })
    regularProxies;
}
