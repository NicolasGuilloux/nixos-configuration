{ lib, ... }:

with lib;
with types;

let
  ssoProviders = [ "authelia" "authentik" ];
in
{
  options.services.reverseProxies = {
    default = {
      sso = {
        enable = mkOption {
          type = bool;
          default = false;
          example = true;
          description = "Enable SSO";
        };

        provider = mkOption {
          type = enum ssoProviders;
          example = "authentik";
          description = "SSO provider";
        };
      };
    };

    services = mkOption {
      default = { };
      description = "Add a reverse proxy";

      # services.reverseProxies.services
      type = attrsOf (submodule ({ ... }: {
        options = {
          enable = mkOption {
            type = bool;
            default = true;
            example = false;
            description = "Enable the reverse proxy";
          };

          oci-container = {
            name = mkOption {
              type = nullOr str;
              default = null;
              example = "nextcloud";
              description = "Name of OCI Container to put the labels on";
            };

            port = mkOption {
              type = int;
              default = 80;
              example = 443;
              description = "Port inside of OCI Container to point the reverse proxy";
            };
          };

          url = mkOption {
            type = str;
            default = "http://127.0.0.1";
            example = "http://test.eu";
            description = "URL to point";
          };

          port = mkOption {
            type = nullOr int;
            default = null;
            example = 80;
            description = "Port to point";
          };

          subdomain = mkOption {
            type = nullOr str;
            example = "nextcloud";
            description = "Subdomain";
          };

          domain = mkOption {
            type = str;
            default = "nicolasguilloux.eu";
            example = "nicolasguilloux.eu";
            description = "Domain";
          };

          tlsChallenge = mkOption {
            type = str;
            default = "acme-challenge";
            example = "acme-challenge";
            description = "TLS Challenge";
          };

          middlewares = mkOption {
            type = listOf str;
            default = [ ];
            description = "List of middlewares to apply to the service";
          };

          provider = mkOption {
            type = nullOr (enum [ "ovh" "cloudflare" ]);
            default = null;
            example = "ovh";
            description = "Provider to let terraform declare the domain automatically";
          };

          sso = {
            enable = mkOption {
              type = bool;
              example = true;
              # default = cfgDefault.sso.enable;
              default = true;
              description = "Enable SSO";
            };

            provider = mkOption {
              type = enum ssoProviders;
              # default = cfgDefault.sso.default;
              default = "authentik";
              example = "authelia";
              description = "SSO provider";
            };

            excludePathPrefixes = mkOption {
              type = listOf str;
              default = [ ];
              example = [ "/api" ];
              description = "Exclude some paths from SSO validation";
            };
          };
        };
      }));
    };
  };
}
