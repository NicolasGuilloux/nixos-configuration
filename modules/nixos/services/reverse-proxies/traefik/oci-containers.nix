{ config, lib, ... }:

let
  proxies = lib.filterAttrs (_key: value: value.enable && value.oci-container.name != null) config.services.reverseProxies.services;
in
{
  # https://doc.traefik.io/traefik/routing/providers/docker/
  virtualisation.oci-containers.containers = lib.mapAttrs'
    (
      name: value:
        {
          name = "${value.oci-container.name}";
          value = {
            labels = {
              "traefik.enable" = "true";
              "traefik.http.routers.${name}.entrypoints" = "websecure";
              "traefik.http.routers.${name}.rule" = "Host(`${name}.${value.domain}`)";
              "traefik.http.services.${name}.loadbalancer.server.port" = toString value.oci-container.port;
            };
            extraOptions = [
              (if (value.domain != "local") then
                if value.provider == "cloudflare" then
                  "--label=traefik.http.routers.${name}.tls.certresolver=cloudflare"
                else
                  "--label=traefik.http.routers.${name}.tls.certresolver=${value.tlsChallenge}"
              else
                "--label=traefik.http.routers.${name}.tls=true"
              )
            ] ++ lib.map
              (middleware:
                "--label=traefik.http.routers.${name}.middlewares=${middleware}"
              )
              value.middlewares;
          };
        }
    )
    proxies;
}
