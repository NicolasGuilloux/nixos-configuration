{ config, lib, ... }:

let
  regularProxies = lib.filterAttrs (_key: value: value.enable && value.oci-container.name == null) config.services.reverseProxies.services;
in
{
  # Creates routers
  services.traefik.dynamicConfigOptions.http.routers = lib.mapAttrs'
    (
      name: value:
        let
          outsideUrl = "${name}.${value.domain}";
        in
        {
          name = "${outsideUrl}";
          value = {
            rule = "Host(`${outsideUrl}`)";
            service = "${outsideUrl}";
            tls =
              if (value.domain == "local") then
                true
              else
                {
                  certResolver =
                    if value.provider == "cloudflare" then
                      "cloudflare"
                    else
                      value.tlsChallenge;
                };
            entryPoints = [ "websecure" ];
            middlewares = [ "rate-limit" "secure-headers" ] ++ value.middlewares;
          };
        }
    )
    regularProxies;

  # Creates services
  services.traefik.dynamicConfigOptions.http.services = lib.mapAttrs'
    (
      name: value:
        let
          outsideUrl = "${name}.${value.domain}";
        in
        {
          name = "${outsideUrl}";
          value = {
            loadBalancer = {
              servers = [
                { url = "${value.url}\:${toString value.port}"; }
              ];
            };
          };
        }
    )
    regularProxies;
}
