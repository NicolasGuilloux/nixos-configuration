{ config, lib, ... }:

let
  proxies = lib.filterAttrs (_key: value: value.enable && value.sso.enable && value.sso.provider == "authentik") config.services.reverseProxies.services;
  regularProxies = lib.filterAttrs (_key: value: value.oci-container.name == null) proxies;
  ociContainerProxies = lib.filterAttrs (_key: value: value.oci-container.name != null) proxies;
in
{
  # Creates routers
  services.traefik.dynamicConfigOptions.http.routers = lib.mapAttrs'
    (
      name: value:
        let
          outsideUrl = "${name}.${value.domain}";
        in
        {
          name = "${outsideUrl}";
          value = {
            middlewares = [ "authentik" ];
          };
        }
    )
    regularProxies;

  # https://doc.traefik.io/traefik/routing/providers/docker/
  virtualisation.oci-containers.containers = lib.mapAttrs'
    (
      name: value:
        {
          name = "${value.oci-container.name}";
          value =
            {
              extraOptions = [
                "--label=traefik.http.routers.${name}.middlewares=authentik@file"
              ];
            };
        }
    )
    ociContainerProxies;
}
