{ config, lib, ... }:

let
  proxies = lib.filterAttrs (_key: value: value.enable && value.sso.enable && value.sso.provider == "authentik" && value.sso.excludePathPrefixes != [ ]) config.services.reverseProxies.services;
  regularProxies = lib.filterAttrs (_key: value: value.oci-container.name == null) proxies;
  ociContainerProxies = lib.filterAttrs (_key: value: value.oci-container.name != null) proxies;
in
{
  # Creates routers
  services.traefik.dynamicConfigOptions.http.routers = lib.mapAttrs'
    (
      name: value:
        let
          outsideUrl = "${name}.${value.domain}";
          parentCfg = config.services.traefik.dynamicConfigOptions.http.routers."${outsideUrl}";
          extraRule = lib.concatStringsSep " || " (builtins.map (value: "PathPrefix(`" + value + "`)") value.sso.excludePathPrefixes);
        in
        {
          name = "noauth-${outsideUrl}";
          value = parentCfg // {
            rule = lib.mkAfter "(${parentCfg.rule}) && (${extraRule})";
            middlewares = lib.mkForce (builtins.filter (value: value != "authentik") parentCfg.middlewares);
          };
        }
    )
    regularProxies;

  # https://doc.traefik.io/traefik/routing/providers/docker/
  virtualisation.oci-containers.containers = lib.mapAttrs'
    (
      name: value:
        {
          name = "${value.oci-container.name}";
          value =
            let
              extraRule = lib.concatStringsSep " || " (builtins.map (value: "PathPrefix(`" + value + "`)") value.sso.excludePathPrefixes);
            in
            {
              extraOptions = [
                "--label=traefik.http.middlewares.${name}-middleware-chain-noauth.chain.middlewares=${builtins.concatStringsSep "," value.middlewares}"
                "--label=traefik.http.routers.noauth-${name}.entrypoints=websecure"
                "--label=traefik.http.routers.noauth-${name}.rule=Host(`${name}.${value.domain}`) && (${extraRule})"
                "--label=traefik.http.routers.noauth-${name}.tls=true"
                "--label=traefik.http.routers.noauth-${name}.tls.certresolver=acme-challenge"
                "--label=traefik.http.routers.noauth-${name}.middlewares=${name}-middleware-chain-noauth"
              ];
            };
        }
    )
    ociContainerProxies;
}
