{ config, lib, ... }:

let
  proxies = lib.filterAttrs (_key: value: value.enable) config.services.reverseProxies.services;
in
{
  # Add TLS for designated domains
  services.traefik.staticConfigOptions.entryPoints.websecure.http.tls = {
    certResolver = "cloudflare";
    domains = [
      {
        main = "nicolasguilloux.eu";
        sans = [ "localhost" "127.0.0.1" ] ++
          lib.mapAttrsToList
            (name: value: "${name}.${value.domain}")
            (lib.filterAttrs (_key: value: value.provider == "cloudflare") proxies);
      }
    ];
  };
}
