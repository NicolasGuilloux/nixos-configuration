{ config, lib, pkgs, ... }:

let
  cfg = config.services.audible-downloader;

  downloadScript = pkgs.writeShellScriptBin "download-audible" ''
    # Variables
    id="$1"
    downloadDir="${cfg.downloadDir}/$id"
    outputDir="${cfg.exportDir}/$id"

    # Folders
    mkdir -p "$downloadDir"
    mkdir -p "$outputDir"
    cd "$downloadDir"

    # Download the decrypt plugin if necessary
    if [ ! -f "${cfg.dataDir}/.audible/plugins/cmd_decrypt.py" ]; then
      mkdir -p "${cfg.dataDir}/.audible/plugins"
      wget https://raw.githubusercontent.com/mkb79/audible-cli/master/plugin_cmds/cmd_decrypt.py -O "${cfg.dataDir}/.audible/plugins/cmd_decrypt.py"
    fi

    printf "\n\n------ Downloading Audible book $id ------\n"
    audible download \
      --output-dir "$downloadDir" \
      --aaxc \
      --no-confirm \
      -a "$id"

    if [ -z "$(ls -A ./)" ]; then
      audible download \
        --output-dir "$downloadDir" \
        --aax \
        --no-confirm \
        -a "$id"
    fi

    printf "\n\n------ Converting Audible book $id ------\n"
    cd "$downloadDir"
    audible decrypt \
      --all \
      --dir "$outputDir"
  '';
in
{
  imports = [ ./config.nix ];

  config = lib.mkIf cfg.enable {
    environment.systemPackages = [
      cfg.audibleCliPackage
      cfg.ffmpegPackage
      downloadScript
    ];

    # User
    users.users = lib.mkIf (cfg.user == "audible") {
      audible = {
        createHome = true;
        home = cfg.dataDir;
        description = "Audible user account";
        group = cfg.group;
        isSystemUser = true;
      };
    };

    # Group
    users.groups = lib.mkIf (cfg.group == "audible") {
      audible.name = "audible";
    };

    # Download service
    systemd.services.audible-downloader = {
      enable = true;
      description = "Download Audible books.";
      wantedBy = [ "multi-user.target" ];
      requires = [ "network-online.target" ];
      after = [ "network-online.target" ];
      serviceConfig = {
        Restart = "on-failure";
        User = cfg.user;
        Group = cfg.group;
      };
      path = [
        cfg.audibleCliPackage
        cfg.ffmpegPackage
        pkgs.wget
      ];
      script = ''
        FILELOG="${cfg.dataDir}/processed_titles.txt"
        touch "$FILELOG"

        ASINS=$(${cfg.audibleCliPackage}/bin/audible library list | ${pkgs.ripgrep}/bin/rg -o "[0-9A-Z]{10}")
        printf "\nAvailable asins:\n$ASINS\n"

        for asin in $ASINS; do
          if grep -Fq $asin "$FILELOG"
          then
              continue
          fi

          ${downloadScript}/bin/download-audible $asin
          echo "$asin" >> "$FILELOG"
        done
      '';
    };
  };
}
