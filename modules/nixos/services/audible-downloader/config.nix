{ config, lib, pkgs, ... }:

with lib;
with types;

let
  cfg = config.services.audible-downloader;
in
{
  options.services.audible-downloader = {
    enable = mkOption {
      type = types.bool;
      default = false;
      description = "Enable Audible downloader service";
    };

    user = mkOption {
      type = str;
      default = "audible";
      description = "User for the service";
    };

    group = mkOption {
      type = str;
      default = "audible";
      description = "Group for the service";
    };

    dataDir = mkOption {
      type = str;
      default = "/var/lib/audible";
      description = "Location of the data directory";
    };

    downloadDir = mkOption {
      type = str;
      default = "${cfg.dataDir}/downloads";
      description = "Location of the download directory (aax files)";
    };

    exportDir = mkOption {
      type = str;
      default = "${cfg.dataDir}/export";
      description = "Location of the export directory (m4b files)";
    };

    audibleCliPackage = mkOption {
      type = package;
      default = pkgs.audible-cli;
      description = "audible-cli package to use";
    };

    ffmpegPackage = mkOption {
      type = package;
      default = pkgs.ffmpeg;
      description = "ffmpeg package to use";
    };
  };
}
