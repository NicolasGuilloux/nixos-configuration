{ config, lib, ... }:

let
  cfg = config.services.homer;

  toList = lib.mapAttrsToList (name: value: value // { name = name; });
  sortValues = attrset:
    builtins.sort
      (a: b: a.order < b.order)
      (toList attrset);

  sanatizeCategory = category:
    builtins.removeAttrs
      (category // { items = builtins.map sanatizeService (sortValues category.services); })
      [ "services" "order" ];

  sanatizeService = service:
    builtins.removeAttrs
      (service // service.customService // {
        keywords = builtins.concatStringsSep " " service.keywords;
      })
      [ "order" "customService" ];
in
{
  services.homer.config.services = builtins.map sanatizeCategory (sortValues cfg.categories);
}
