{ config, pkgs, ... }:

let
  cfg = config.services.homer;
in
{
  imports = [ ./config.nix ./services.nix ];

  # Declare the container
  virtualisation.oci-containers.containers.homer = {
    image = "b4bz/homer";
    ports = [ "${toString cfg.port}:8080" ];
    volumes = [
      "${cfg.dataDir}:/www/assets"
      "${pkgs.writeText "config.yml" (builtins.toJSON cfg.config)}:/www/assets/config.yml"
    ];
  };

  # Firewall
  networking.firewall.allowedTCPPorts = if cfg.openFirewall then [ cfg.port ] else [ ];
}
