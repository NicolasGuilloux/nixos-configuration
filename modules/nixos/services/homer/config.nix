{ lib, pkgs, ... }:

with lib;
with types;

let
  categoryConfig = {
    icon = mkOption {
      type = str;
      example = "fas fa-home";
      description = "Icon of the category";
    };

    order = mkOption {
      type = int;
      default = 9999;
      description = "Order to display the category";
    };

    services = mkOption {
      default = { };
      description = "List of services";
      type = attrsOf (submodule ({ ... }: {
        options = serviceConfig;
      }));
    };
  };

  serviceConfig = {
    subtitle = mkOption {
      type = nullOr str;
      default = null;
      description = "Subtitle of the service";
    };

    logo = mkOption {
      type = str;
      description = "URL to the logo";
    };

    url = mkOption {
      type = str;
      description = "URL to the service";
    };

    keywords = mkOption {
      type = listOf str;
      default = [ ];
      description = "Keywords to describe your service";
    };

    tag = mkOption {
      type = nullOr str;
      default = null;
      description = "Tags to describe your service";
    };

    target = mkOption {
      type = nullOr str;
      default = "_blank";
      description = "Extra HTML tag";
    };

    order = mkOption {
      type = int;
      default = 9999;
      description = "Order to display the service";
    };

    customService = mkOption {
      default = { };
      type = attrsOf (pkgs.formats.yaml { }).type;
      description = "Custom service data";
    };
  };
in
{
  options.services.homer = {
    enable = mkOption {
      type = bool;
      default = false;
      example = true;
      description = "Enable Homer service";
    };

    openFirewall = mkOption {
      type = bool;
      default = false;
      example = true;
      description = "Open firewall for the service";
    };

    port = mkOption {
      type = int;
      default = 8080;
      description = "Port of the app";
    };

    dataDir = mkOption {
      type = path;
      default = "/var/lib/homer";
      description = "Where to store the data";
    };

    categories = mkOption {
      default = { };
      description = "All the categories";
      type = attrsOf (submodule ({ ... }: {
        options = categoryConfig;
      }));
    };

    config = mkOption {
      default = { };
      type = attrsOf (pkgs.formats.yaml { }).type;
      description = "Configuration of Homer";
    };
  };
}
