{ config, lib, pkgs, ... }:

let
  cfg = config.services.terraform;
in
{
  system.activationScripts.terraform-config.text = lib.mkAfter (
    (lib.concatStringsSep "\n"
      (lib.mapAttrsToList
        (name: value:
          let
            terraformConfig = pkgs.writeText
              "config.${name}.tf.json"
              (builtins.toJSON value.config);
          in
          ''
            mkdir -p "${cfg.dataDir}/${name}"
            ln -Tfs ${terraformConfig} "${cfg.dataDir}/${name}/config.tf.json"
          ''
        )
        (lib.filterAttrs (_key: value: value.config != { }) cfg.configs)
      )
    )
  );
}
