{ config, lib, pkgs, ... }:

let
  cfg = config.services.terraform;
in
{
  systemd.services = lib.mkIf cfg.enable (lib.mapAttrs'
    (name: value:
      {
        name = "terraform-apply-${name}";
        value = {
          enable = value.autoApply;
          description = "Apply Terraform configuration for ${name}";
          wantedBy = [ "multi-user.target" ];
          requires = [ "terraform-init-${name}.service" ];
          after = [ "terraform-init-${name}.service" ];
          path = [ cfg.package pkgs.glibc ];
          script = "${cfg.package}/bin/terraform apply -auto-approve";
          serviceConfig = {
            WorkingDirectory = "${cfg.dataDir}/${name}";
            Type = "oneshot";
          };
        };
      }
    )
    (lib.filterAttrs (_key: value: value.config != { }) cfg.configs)
  );
}
