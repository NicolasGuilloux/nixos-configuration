{ config, lib, ... }:

let
  cfg = config.services.terraform;
in
{
  systemd.services = lib.mapAttrs'
    (name: value:
      {
        name = "terraform-init-${name}";
        value = lib.mkIf cfg.enable {
          enable = true;
          description = "Init Terraform configuration for ${name}";
          wantedBy = [ "multi-user.target" ];
          path = [ cfg.package ];
          script = "${cfg.package}/bin/terraform init";
          serviceConfig = {
            WorkingDirectory = "${cfg.dataDir}/${name}";
            Type = "oneshot";
          };
        };
      }
    )
    (lib.filterAttrs (_key: value: value.config != { }) cfg.configs);
}
