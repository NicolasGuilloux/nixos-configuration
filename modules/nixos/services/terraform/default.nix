{ config, ... }:

let
  cfg = config.services.terraform;
in
{
  imports = [ ./config.nix ./apply.nix ./bind-configs.nix ./init.nix ];

  environment.systemPackages = if cfg.enable then [ config.services.terraform.package ] else [ ];
}
