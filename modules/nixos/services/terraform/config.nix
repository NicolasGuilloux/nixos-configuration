{ lib, pkgs, ... }:

with lib;
with types;

let format = pkgs.formats.yaml { };
in
{
  options.services.terraform = {
    enable = lib.mkEnableOption "Enable Terraform";
    enableIPv4 = mkEnableOption "Enable IPv4 support";
    enableIPv6 = mkEnableOption "Enable IPv6 support";

    package = mkOption {
      type = package;
      default = pkgs.terraform;
      description = "Package used to mount the directory";
    };

    dataDir = mkOption {
      type = str;
      default = "/var/lib/terraform";
      description = "The directory where Terraform stores its files.";
    };

    configs = mkOption {
      default = { };
      description = "Configuration of a Terraform";

      type = attrsOf (submodule ({ config, ... }: {
        options = {
          autoApply = lib.mkEnableOption "Apply Terraform configuration automatically on edition";

          config = mkOption {
            type = format.type;
            description = "A free form of the Terraform configuration";
          };
        };
      }));
    };
  };
}
