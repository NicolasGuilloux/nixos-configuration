{ config, lib, ... }:

let cfg = config.services.cloudflared;
in
{
  config = lib.mkIf cfg.enable {
    environment.systemPackages = [ cfg.package ];

    # Default params
    services.cloudflared.tunnels."${config.networking.hostName}" = {
      default = lib.mkDefault "http_status:404";
      credentialsFile = lib.mkDefault config.age.secrets.cloudflared-credentials.path;
    };

    # Set a home for the user
    users.users = lib.mkIf (config.services.cloudflared.user == "cloudflared") {
      cloudflared = {
        home = "/var/lib/cloudflared";
        createHome = true;
      };
    };

    # Agenix password
    age.secrets.cloudflared-credentials = {
      owner = config.services.cloudflared.user;
      group = config.services.cloudflared.group;
    };
  };
}
