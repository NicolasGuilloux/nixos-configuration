{ lib, ... }:

with lib;
with types;

/** Resources: https://gist.github.com/CRTified/43b7ce84cd238673f7f24652c85980b3 */
{
  options.virtualisation.libvirtd.virtualMachines = lib.mkOption {
    # virtualisation.libvirtd.virtualMachines.<name>
    type = attrsOf (submodule {
      options = {
        unbindVTconsoles = mkEnableOption "Unbind VTConsoles";
        unbindEfiFramebuffer = mkEnableOption "Unbind EFI Framebuffer";

        # virtualisation.libvirtd.virtualMachines.<name>.cpuAllocation
        cpuAllocation = {
          enable = mkEnableOption "Dedicate the first core to the host, and the rest to the guest";
          totalThreadsCount = mkOption {
            type = int;
            example = 8;
            description = "Total cores count of your machine";
          };

          threadsAllocated = mkOption {
            type = int;
            example = 4;
            description = "Cores count to allocate to your guest";
          };
        };

        # virtualisation.libvirtd.virtualMachines.<name>.raceConditionTimeout
        raceConditionTimeout = mkOption {
          type = int;
          default = 0;
          example = 2;
          description = "Avoid race condition by simply sleeping before unloading kernel modules";
        };

        # virtualisation.libvirtd.virtualMachines.<name>.detachIommuGroups
        detachIommuGroups = mkOption {
          type = listOf int;
          example = [ 14 ];
          default = [ ];
          description = "Detach all devices from the host when the VM starts. The devices are reattached when the VM stops.";
        };

        # virtualisation.libvirtd.virtualMachines.<name>.detachPciAddresses
        detachPciAddresses = mkOption {
          type = listOf int;
          example = [ "pci_0000_02_00_0" ];
          default = [ ];
          description = "Detach all PCI devices designated by their address from the host when the VM starts. The devices are reattached when the VM stops.";
        };

        # virtualisation.libvirtd.virtualMachines.<name>.kernelModules
        kernelModules = {
          # virtualisation.libvirtd.virtualMachines.<name>.kernelModules.load
          load = mkOption {
            type = listOf str;
            default = [ ];
            example = [ "vfio" "vfio_iommu_type1" "vfio_pci" ];
            description = "Kernel modules to load when the VM starts. These kernel modules will be unloaded when the VM stops.";
          };

          # virtualisation.libvirtd.virtualMachines.<name>.kernelModules.unload
          unload = mkOption {
            type = listOf str;
            default = [ ];
            description = "Kernel modules to unload when the VM starts. These kernel modules will be reloaded when the VM stops.";
          };
        };

        # virtualisation.libvirtd.virtualMachines.<name>.services
        services = {
          # virtualisation.libvirtd.virtualMachines.<name>.services.start
          start = mkOption {
            type = listOf str;
            default = [ ];
            description = "Systemd services to start when the VM starts. These services will be shutdown when the VM stops.";
          };

          # virtualisation.libvirtd.virtualMachines.<name>.services.stop
          stop = mkOption {
            type = listOf str;
            default = [ ];
            example = [ "display-manager.service" ];
            description = "Systemd services to stop when the VM starts. These services will be started again when the VM stops.";
          };
        };
      };
    });
  };
}
