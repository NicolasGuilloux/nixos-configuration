{ config, lib, pkgs, ... }:

let
  vmHooks = vmName: vmConfig: [
    {
      name = "gpu_passthrough";
      vmName = vmName;
      stage = "prepare";
      script = pkgs.callPackage ./assets/start.nix { inherit vmName vmConfig; };
    }
    {
      name = "gpu_passthrough";
      vmName = vmName;
      stage = "release";
      executeAfter = true;
      script = pkgs.callPackage ./assets/stop.nix { inherit vmConfig vmName; };
    }
  ];
in
{
  imports = [ ./config.nix ];

  virtualisation.libvirtd.domainHooks = lib.concatLists (
    lib.mapAttrsToList
      vmHooks
      config.virtualisation.libvirtd.virtualMachines
  );

  assertions = lib.mapAttrsToList
    (name: cfg: {
      assertion = (cfg.cpuAllocation.totalThreadsCount - cfg.cpuAllocation.threadsAllocated) > 0;
      message = "The VM ${name} should not take more core than the maximum available and letting 1 to the host";
    })
    config.virtualisation.libvirtd.virtualMachines;
}
