{ pkgs, vmName, vmConfig, ... }:

let
  concatAndMap = function: list: builtins.concatStringsSep "\n" (map function list);
  optional = predicate: text: if predicate then text else "";
in
pkgs.writeShellScript "start-${vmName}-hook.sh" ''
  # Stop services
  ${concatAndMap (service: "systemctl stop ${service}") vmConfig.services.stop}

  ${optional vmConfig.unbindVTconsoles ''
    # Unbind VTconsoles
    echo 0 > /sys/class/vtconsole/vtcon0/bind
    echo 0 > /sys/class/vtconsole/vtcon1/bind
  ''}

  ${optional vmConfig.unbindEfiFramebuffer ''
    # Unbind EFI Framebuffer
    echo efi-framebuffer.0 > /sys/bus/platform/drivers/efi-framebuffer/unbind
  ''}

  # Avoid race condition
  sleep ${toString vmConfig.raceConditionTimeout}

  # Unload kernel modules
  ${concatAndMap (module: "modprobe --remove-holders -r ${module}") vmConfig.kernelModules.unload}

  # Detach IOMMU Groups
  ${concatAndMap (group: "${pkgs.nover.iommu-groups}/bin/iommu-groups detach ${toString group}") vmConfig.detachIommuGroups}

  # Detach PCI addresses
  ${concatAndMap (pciAddress: "virsh nodedev-detach ${pciAddress}") vmConfig.detachPciAddresses}

  # Start services
  ${concatAndMap (service: "systemctl start ${service}") vmConfig.services.start}

  # Load kernel modules
  ${concatAndMap (module: "modprobe ${module}") vmConfig.kernelModules.load}

  ${optional vmConfig.cpuAllocation.enable ''
    # Isolate host to first core
    ${pkgs.nover.cpu-manipulation}/bin/cpu-manipulation reserve ${toString vmConfig.cpuAllocation.threadsAllocated}
  ''}
''
