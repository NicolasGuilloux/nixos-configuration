{ pkgs, vmName, vmConfig, ... }:

let
  concatAndMap = function: list: builtins.concatStringsSep "\n" (map function list);
  optional = predicate: text: if predicate then text else "";
in
pkgs.writeShellScript "stop-${vmName}-hook.sh" ''
  # Return host to all cores
  ${optional vmConfig.cpuAllocation.enable ''
    ${pkgs.nover.cpu-manipulation}/bin/cpu-manipulation free
  ''}
  
  # Stop services
  ${concatAndMap (service: "systemctl stop ${service}") vmConfig.services.start}

  # Avoid race condition
  sleep ${toString vmConfig.raceConditionTimeout}

  # Unload kernel modules
  ${concatAndMap (module: "modprobe --remove-holders -r ${module}") vmConfig.kernelModules.load}

  # Reattach IOMMU Groups
  ${concatAndMap (group: "${pkgs.nover.iommu-groups}/bin/iommu-groups attach ${toString group}") vmConfig.detachIommuGroups}

  # Reattach PCI addresses
  ${concatAndMap (pciAddress: "virsh nodedev-reattach ${pciAddress}") vmConfig.detachPciAddresses}

  # Reload kernel modules
  ${concatAndMap (module: "modprobe ${module}") vmConfig.kernelModules.unload}

  # Avoid race condition
  sleep ${toString vmConfig.raceConditionTimeout}

  ${optional vmConfig.unbindEfiFramebuffer ''
    # Rebind EFI Framebuffer
    echo efi-framebuffer.0 > /sys/bus/platform/drivers/efi-framebuffer/bind
  ''}

  ${optional vmConfig.unbindVTconsoles ''
    # Rebind VTconsoles
    echo 1 > /sys/class/vtconsole/vtcon0/bind
    echo 1 > /sys/class/vtconsole/vtcon1/bind
  ''}

  # Restart services
  ${concatAndMap (service: "systemctl start ${service}") vmConfig.services.stop}
''
