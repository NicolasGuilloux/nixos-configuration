{ lib, ... }:

with lib;
with types;

{
  options.services.uptime-kuma = {
    port = lib.mkOption {
      type = int;
      default = 3001;
      description = "Main subdomain of the system";
    };

    dataDir = lib.mkOption {
      type = str;
      default = "/var/lib/uptime-kuma";
      description = "Data directory of the service";
    };
  };
}
