{ config, lib, ... }:

let cfg = config.services.uptime-kuma;
in
{
  imports = lib.snowfall.fs.get-non-default-nix-files-recursive ./.;

  services.uptime-kuma.settings = {
    PORT = toString cfg.port;
    DATA_DIR = lib.mkForce cfg.dataDir;
  };
}
