{ lib, ... }:

with lib;
with types;

{
  options.networking.mainSubdomain = lib.mkOption {
    type = nullOr str;
    default = null;
    description = "Main subdomain of the system";
  };
}
