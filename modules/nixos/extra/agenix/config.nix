{ lib, ... }:

with lib;
with types;

{
  options.age = {
    folders = mkOption {
      type = listOf path;
      default = [ ];
      description = "List of folders that contains Age files to import automagically.";
    };

    secretName = mkOption {
      # type = ;
      default = path: lib.removeSuffix ".age" (builtins.baseNameOf path);
      description = "Lambda that generates the name of the secret from the Age file path";
    };
  };
}
