{ config, lib, ... }:

let
  cfg = config.age;

  # Get all Age files in folder
  get-age-files = folder: builtins.filter
    (lib.hasSuffix ".age")
    (lib.snowfall.fs.get-files folder);

  # Get all Age files in each folders
  ageFiles = lib.flatten (
    builtins.map
      (folder: get-age-files folder)
      cfg.folders
  );
in
{
  age.secrets = lib.nover.mapToAttrs
    (file:
      {
        name = cfg.secretName (builtins.unsafeDiscardStringContext file);
        value = { inherit file; };
      }
    )
    ageFiles;
}
