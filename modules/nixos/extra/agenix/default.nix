{ lib, inputs, ... }:

{
  imports = lib.snowfall.fs.get-non-default-nix-files-recursive ./.;

  environment.systemPackages = [ inputs.agenix.packages.x86_64-linux.agenix ];
}
