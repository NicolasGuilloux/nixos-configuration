{ lib, ... }:

{
  imports = lib.snowfall.fs.get-non-default-nix-files-recursive ./.;

  # Main configuraion
  services.traefik.staticConfigOptions = {
    log.filePath = "/var/log/traefik/traefik.log";
    log.level = "DEBUG";
    accessLog.filePath = "/var/log/traefik/accessLog.log";

    entryPoints = {
      websecure.address = ":443";
      web = {
        address = ":80";
        # http.redirections.entryPoint = {
        #   to = "websecure";
        #   scheme = "https";
        # };
      };
    };
  };

  # Placeholder to avoid error
  services.traefik.dynamicConfigOptions.http.services.dummy.loadBalancer.servers = [ ];
}
