{ ... }:

{
  services.traefik.dynamicConfigOptions.http.middlewares.rate-limit.rateLimit = {
    average = 200;
    burst = 100;
  };
}
