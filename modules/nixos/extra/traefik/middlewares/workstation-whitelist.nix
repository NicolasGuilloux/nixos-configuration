{ config, lib, ... }:

let
  extractIps = property: lib.flatten
    (lib.mapAttrsToList
      (_slug: device: device."${property}")
      (lib.filterAttrs
        (_slug: device: device.isWorkstation && device."${property}" != null)
        config.devices)
    );

  ips = ([ "127.0.0.1" ] ++ extractIps "localIps");
in
{
  services.traefik.dynamicConfigOptions.http.middlewares.workstation-whitelist.ipwhitelist.sourcerange = ips;
}
