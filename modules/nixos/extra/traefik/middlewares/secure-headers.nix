{ ... }:

{
  services.traefik.dynamicConfigOptions.http.middlewares.secure-headers.headers = {
    accessControlAllowMethods = [ "GET" "OPTIONS" "PUT" "POST" ];
    accessControlAllowOriginListRegex = [ "https://(.*)?nicolasguilloux\\.eu(.*)" ];
    accessControlMaxAge = 100;
    hostsProxyHeaders = [ "X-Forwarded-Host" ];
    sslProxyHeaders.X-Forwarded-Proto = "https";
    sslRedirect = true;
    stsSeconds = 63072000;
    stsIncludeSubdomains = true;
    stsPreload = true;
    forceSTSHeader = true;
    contentTypeNosniff = true;
    browserXssFilter = true;
    referrerPolicy = "same-origin";
    customResponseHeaders.X-Robots-Tag = "none";
    # featurePolicy= """
    #   camera 'none'; geolocation 'none'; microphone 'none'; payment 'none';
    #   usb 'none'; vr 'none'
    # """;
  };
}
