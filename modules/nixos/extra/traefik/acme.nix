{ config, lib, ... }:

let
  cfg = config.services.traefik;
in
{
  services.traefik.staticConfigOptions.certificatesResolvers.acme-challenge.acme =
    lib.mkIf
      config.security.acme.acceptTerms
      {
        email = config.security.acme.defaults.email;
        storage = "${cfg.dataDir}/acme.json";
        httpChallenge.entryPoint = "web";
      };
}
