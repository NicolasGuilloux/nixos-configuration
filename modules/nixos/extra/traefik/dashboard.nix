{ config, lib, ... }:

{
  # Configure
  services.traefik.staticConfigOptions = {
    api.dashboard = lib.mkDefault true;
    api.insecure = lib.mkDefault false;
  };

  # Routing
  services.traefik.dynamicConfigOptions.http.routers.dashboard = {
    rule = lib.mkDefault "Host(`traefik.local`)";
    service = "api@internal";
    entryPoints = [ "web" "websecure" ];
    tls = lib.mkDefault true;
  };

  # Add Dashboard to hosts
  networking.hosts."127.0.0.1" =
    if config.services.traefik.dynamicConfigOptions.http.routers.dashboard.rule == "Host(`traefik.local`)" then
      [ "traefik.local" ]
    else
      [ ];
}
