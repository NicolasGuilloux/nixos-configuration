{ config, lib, ... }:

let
  cfg = config.services.traefik;
in
{
  services.traefik = lib.mkIf cfg.enableDocker {
    group = "docker";
    staticConfigOptions.providers.docker = {
      endpoint = "unix:///run/docker.sock";
      watch = true;
      exposedByDefault = false;
    };
  };
}
