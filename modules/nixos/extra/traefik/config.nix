{ lib, ... }:

with lib;
with types;

{
  options.services.traefik = {
    enableDocker = mkEnableOption "Enable Docker support.";
  };
}
