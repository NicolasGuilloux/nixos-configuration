{ config, pkgs, lib, ... }:

let
  wolInterfaces = lib.filterAttrs
    (_name: value: value.wakeOnLan.enable)
    config.networking.interfaces;
in
with lib;
{
  config = mkIf (wolInterfaces != { }) {
    networking.firewall.allowedUDPPorts = [ 9 ];

    services.cron.enable = true;
    services.cron.systemCronJobs =
      lib.mapAttrsToList
        (name: _value: "@reboot root ${pkgs.ethtool}/sbin/ethtool -s ${name} wol g")
        wolInterfaces;
  };
}
