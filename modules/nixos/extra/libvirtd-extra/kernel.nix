{ config, lib, ... }:

let
  cfg = config.virtualisation.libvirtd;

  iommuTypeKernelParams =
    if cfg.IOMMUType == "intel" then
      [ "intel_iommu=on" "intel_iommu=igfx_off" ]
    else if cfg.IOMMUType == "amd" then
      [ "amd_iommu=on" ]
    else
      [ ];
in
{
  config = lib.mkIf cfg.enable {
    boot.kernelParams = [ "iommu=pt" "kvm.ignore_msrs=1" "kvm.report_ignored_msrs=0" ] ++ iommuTypeKernelParams;
  };
}
