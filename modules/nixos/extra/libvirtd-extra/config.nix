{ lib, ... }:

with lib;
with types;

/** Resources: https://gist.github.com/CRTified/43b7ce84cd238673f7f24652c85980b3 */
{
  options.virtualisation.libvirtd = {
    IOMMUType = mkOption {
      type = types.enum [ "intel" "amd" null ];
      default = null;
      example = "intel";
      description = "Type of the IOMMU used";
    };
  };
}
