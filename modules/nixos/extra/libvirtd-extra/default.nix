{ config, pkgs, lib, ... }:

let
  cfg = config.virtualisation.libvirtd;

  qemuEnvironment = pkgs.buildEnv {
    name = "qemu-hook-env";
    paths = with pkgs; [
      bash
      libvirt
      kmod
      systemd
      pciutils
    ];
  };
in
{
  imports = [ ./config.nix ./kernel.nix ];

  config = lib.mkIf cfg.enable {
    # Solve hooks bash accessibility
    systemd.services.libvirtd.path = [ pkgs.swtpm qemuEnvironment ];
  };
}
