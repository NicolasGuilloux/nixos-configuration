{ lib, ... }:

with lib;
with types;

/** Resources: https://gist.github.com/CRTified/43b7ce84cd238673f7f24652c85980b3 */
{
  options.virtualisation.libvirtd = {
    domainHooks = mkOption {
      default = [ ];
      description = "List of hooks";

      type = listOf (submodule {
        options = {
          name = mkOption {
            type = str;
            description = "Name of the script. Keep in mind that it will execute the in alphabetical order.";
          };

          vmName = mkOption {
            type = str;
            description = "Name of the VM";
          };

          stage = mkOption {
            type = enum [ "prepare" "release" ];
            example = "prepare";
            description = "At what stage the scripts must execute";
          };

          executeAfter = mkOption {
            type = bool;
            default = false;
            example = "true";
            description = "Execute the script after the stage";
          };

          script = mkOption {
            type = path;
            description = "Script to execute";
          };
        };
      });
    };

    virtualMachines = lib.mkOption {
      default = { };
      description = "Configure Virtual Machines";

      # virtualisation.libvirtd.virtualMachines.<name>
      type = attrsOf (submodule ({ ... }: {
        options = {
          # virtualisation.libvirtd.virtualMachines.<name>.hooks
          hooks = {
            # virtualisation.libvirtd.virtualMachines.<name>.hooks.start
            start = {
              # virtualisation.libvirtd.virtualMachines.<name>.hooks.start.before
              before = mkOption {
                type = listOf path;
                default = [ ];
                description = "Script to execute before the VM starts";
              };

              # virtualisation.libvirtd.virtualMachines.<name>.hooks.start.after
              after = mkOption {
                type = listOf path;
                default = [ ];
                description = "Script to execute after the VM starts";
              };
            };

            # virtualisation.libvirtd.virtualMachines.<name>.hooks.stop
            stop = {
              # virtualisation.libvirtd.virtualMachines.<name>.hooks.stop.before
              before = mkOption {
                type = listOf path;
                default = [ ];
                description = "Script to execute before the VM stops";
              };

              # virtualisation.libvirtd.virtualMachines.<name>.hooks.stop.after
              after = mkOption {
                type = listOf path;
                default = [ ];
                description = "Script to execute after the VM stops";
              };
            };
          };
        };
      }));
    };
  };
}
