{ config, pkgs, ... }:

let
  cfg = config.virtualisation.libvirtd;

  getState = config: if config.executeAfter then "end" else "begin";

  hookTemplate = config: {
    name = "${config.vmName}-${config.stage}-${getState config}-${config.name}";
    value = pkgs.writeShellScript config.name ''
      if [ "${config.vmName}" != "$1" ]; then
        echo "Skip vm name"
        exit 0;
      fi

      if [ "${config.stage}" != "$2" ]; then
        echo "Skip stage"
        exit 0;
      fi

      if [ "${getState config}" != "$3" ]; then
        echo "Skip stage state"
        exit 0;
      fi

      folder=/var/log/libvirt/qemu/hooks/${config.vmName}/${config.stage}/${getState config}
      mkdir -p $folder

      # Log everything
      exec 3>&1 4>&2
      trap 'exec 2>&4 1>&3' 0 1 2 3
      exec 1>$folder/${config.name} 2>&1

      # Execute hook script
      ${config.script} 
    '';
  };
in
{
  virtualisation.libvirtd.hooks.qemu = builtins.listToAttrs (map hookTemplate cfg.domainHooks);
}
