{ config, lib, ... }:

let
  cfg = config.security.localCertification;
in
{
  imports = lib.snowfall.fs.get-non-default-nix-files-recursive ./.;

  config = lib.mkIf cfg.enable {
    # Add all hosts redirected to local
    security.localCertification.domains = config.networking.hosts."127.0.0.1";

    # Add certificates systemwide
    security.pki.certificateFiles =
      if (cfg.authority.certFile != null)
      then
        [ cfg.authority.certFile ]
      else
        [ ];
  };
}
