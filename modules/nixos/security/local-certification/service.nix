{ config, lib, pkgs, ... }:

let
  cfg = config.security.localCertification;

  domainsToString = builtins.concatStringsSep " " (
    builtins.map (domain: "\"${domain}\"") cfg.domains
  );
in
{
  systemd.services.local-certification = lib.mkIf cfg.enable {
    enable = true;
    description = "Generates the local certificates.";
    wantedBy = [ "multi-user.target" "traefik.service" ];
    before = [ "traefik.service" ];
    script = ''
      # Bind authority
      mkdir -p "${cfg.dataDir}/authority"

      ${if (cfg.authority.keyFile == null || cfg.authority.certFile == null) then "" else ''
        ln -fs "${cfg.authority.certFile}" "${cfg.dataDir}/authority/rootCA.pem"
        ln -fs "${cfg.authority.keyFile}" "${cfg.dataDir}/authority/rootCA-key.pem"
      ''}

      # Generate the certificates
      CAROOT="${cfg.dataDir}/authority" \
        ${pkgs.mkcert}/bin/mkcert \
        -cert-file ${cfg.dataDir}/local-cert.pem \
        -key-file ${cfg.dataDir}/local-key.pem \
        ${domainsToString}

      # Fix permission
      chmod 744 ${cfg.dataDir}/*.pem
    '';
  };
}
