{ config, lib, ... }:

let
  cfg = config.security.localCertification;
in
{
  # Add local certificates
  config = lib.mkIf cfg.enable {
    services.traefik.dynamicConfigOptions.tls = {
      certificates = [
        {
          certFile = "${cfg.dataDir}/local-cert.pem";
          keyFile = "${cfg.dataDir}/local-key.pem";
        }
      ];

      stores.default.defaultCertificate = {
        certFile = "${cfg.dataDir}/local-cert.pem";
        keyFile = "${cfg.dataDir}/local-key.pem";
      };
    };
  };
}
