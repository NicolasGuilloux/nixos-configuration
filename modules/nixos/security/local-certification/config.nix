{ lib, ... }:

with lib;
with types;

{
  options.security.localCertification = {
    enable = lib.mkEnableOption "Create a user service that mount pCloud";

    domains = mkOption {
      type = listOf str;
      default = [ ];
      description = "List of all domains supported by the local certification";
    };

    dataDir = mkOption {
      type = str;
      default = "/var/lib/local-certification";
      description = "Directory where everything is stored";
    };

    authority = {
      certFile = mkOption {
        type = nullOr str;
        default = null;
        description = "Authority certificate. The key part must be defined too.";
      };

      keyFile = mkOption {
        type = nullOr str;
        default = null;
        description = "Authority key. The cert part must be defined too.";
      };
    };
  };
}
