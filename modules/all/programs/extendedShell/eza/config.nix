{ lib, pkgs, ... }:

with lib;

{
  options.programs.extendedShell.eza = {
    enable = mkEnableOption "Enable eza shell integration";

    package = mkOption {
      type = types.package;
      default = pkgs.eza;
      description = "The package to use for eza";
    };

    defaultParams = mkOption {
      type = types.listOf types.str;
      default = [ ];
      description = "Default parameters to pass to eza";
    };
  };
}
