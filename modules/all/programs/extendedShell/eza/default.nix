{ config, lib, ... }:

let
  cfg = config.programs.extendedShell.eza;
in
{
  imports = lib.snowfall.fs.get-non-default-nix-files-recursive ./.;

  config = lib.mkIf cfg.enable {
    # Install package
    environment.systemPackages = [ cfg.package ];

    # Set up shell aliases
    environment.shellAliases.ls = "${cfg.package}/bin/eza ${builtins.concatStringsSep " " cfg.defaultParams}";
  };
}
