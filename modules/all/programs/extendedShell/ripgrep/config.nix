{ lib, pkgs, ... }:

with lib;

{
  options.programs.extendedShell.ripgrep = {
    enable = mkEnableOption "Enable ripgrep shell integration";

    package = mkOption {
      type = types.package;
      default = pkgs.ripgrep-all;
      description = "The package to use for ripgrep";
    };

    defaultParams = mkOption {
      type = types.listOf types.str;
      default = [ ];
      description = "Default parameters to pass to ripgrep";
    };
  };
}
