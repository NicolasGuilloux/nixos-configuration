{ config, lib, ... }:

let
  cfg = config.programs.extendedShell.ripgrep;
in
{
  imports = lib.snowfall.fs.get-non-default-nix-files-recursive ./.;

  config = lib.mkIf cfg.enable {
    # Install package
    environment.systemPackages = [ cfg.package ];

    # Set up shell aliases
    environment.shellAliases.grep = "${cfg.package}/bin/rga ${builtins.concatStringsSep " " cfg.defaultParams}";
  };
}
