{ lib, pkgs, ... }:

with lib;

{
  options.programs.extendedShell.bat = {
    enable = mkEnableOption "Enable bat shell integration";

    package = mkOption {
      type = types.package;
      default = pkgs.bat;
      description = "The package to use for bat";
    };

    defaultParams = mkOption {
      type = types.listOf types.str;
      default = [ ];
      description = "Default parameters to pass to bat";
    };
  };
}
