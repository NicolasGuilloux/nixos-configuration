{ config, lib, ... }:

let
  cfg = config.programs.extendedShell.bat;
in
{
  imports = lib.snowfall.fs.get-non-default-nix-files-recursive ./.;

  config = lib.mkIf cfg.enable {
    # Install package
    environment.systemPackages = [ cfg.package ];

    # Set up shell aliases
    environment.shellAliases.cat = "${cfg.package}/bin/bat ${builtins.concatStringsSep " " cfg.defaultParams}";
  };
}
