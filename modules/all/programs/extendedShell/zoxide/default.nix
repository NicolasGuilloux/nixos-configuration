{ config, lib, ... }:

let
  cfg = config.programs.extendedShell.zoxide;
in
{
  imports = lib.snowfall.fs.get-non-default-nix-files-recursive ./.;

  config = lib.mkIf cfg.enable {
    # Install packages
    environment.systemPackages = [ cfg.package cfg.fzfPackage ];

    # Bash integration
    programs.bash.interactiveShellInit = ''
      eval "$(zoxide init --cmd cd bash)"
    '';

    # Zsh integration
    programs.zsh.interactiveShellInit = ''
      eval "$(zoxide init --cmd cd zsh)"
    '';

    # Fish integration
    programs.fish.interactiveShellInit = ''
      zoxide init --cmd cd fish | source
    '';
  };
}
