{ lib, pkgs, ... }:

with lib;

{
  options.programs.extendedShell.zoxide = {
    enable = mkEnableOption "Enable zoxide shell integration";

    package = mkOption {
      type = types.package;
      default = pkgs.zoxide;
      description = "The package to use for zoxide";
    };

    enableInteractive = mkEnableOption "Enable zoxide interactive shell integration";

    fzfPackage = mkOption {
      type = types.package;
      default = pkgs.fzf;
      description = "The fzf package to install";
    };
  };
}
