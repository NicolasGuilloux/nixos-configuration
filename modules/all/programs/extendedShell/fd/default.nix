{ config, lib, ... }:

let
  cfg = config.programs.extendedShell.fd;
in
{
  imports = lib.snowfall.fs.get-non-default-nix-files-recursive ./.;

  config = lib.mkIf cfg.enable {
    # Install package
    environment.systemPackages = [ cfg.package ];

    # Set up shell aliases
    environment.shellAliases.find = "${cfg.package}/bin/fd ${builtins.concatStringsSep " " cfg.defaultParams}";
  };
}
