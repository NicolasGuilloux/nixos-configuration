{ lib, pkgs, ... }:

with lib;

{
  options.programs.extendedShell.fd = {
    enable = mkEnableOption "Enable fd shell integration";

    package = mkOption {
      type = types.package;
      default = pkgs.fd;
      description = "The package to use for fd";
    };

    defaultParams = mkOption {
      type = types.listOf types.str;
      default = [ ];
      description = "Default parameters to pass to fd";
    };
  };
}
