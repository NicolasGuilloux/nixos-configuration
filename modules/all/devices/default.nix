{ config, lib, ... }:

{
  imports = lib.snowfall.fs.get-non-default-nix-files-recursive ./.;
  currentDevice = config.devices."${config.networking.hostName}";
}
