{ lib, ... }:

with lib;
with types;

let
  deviceConfig = {
    enable = mkEnableOption "Enable to select the current device";
    isWorkstation = mkEnableOption "Enable if the device is a workstation";

    theme = mkOption {
      type = str;
      example = "da-one-sea";
      description = "Name of the theme to apply. Check here for te names: https://tinted-theming.github.io/base16-gallery/";
    };

    pathToOsConfig = mkOption {
      type = nullOr str;
      default = null;
      example = "/etc/nixos";
      description = "Absolute path to the OS configuration.";
    };

    publicKeys = mkOption {
      type = listOf str;
      default = [ ];
      description = "List of public keys";
    };

    cacheStorePublicKey = mkOption {
      type = nullOr str;
      default = null;
      description = "nix-serve public key for cache signing";
    };

    publicIp = mkOption {
      type = nullOr str;
      default = null;
      example = "10.100.200.1";
      description = "The local IP of the device";
    };

    localIps = mkOption {
      type = listOf str;
      default = [ ];
      example = [ "192.168.0.0" ];
      description = "The local IPs of the device";
    };

    wireguard = {
      ip = mkOption {
        type = nullOr str;
        default = null;
        description = "Wireguard ip";
      };

      publicKey = mkOption {
        type = nullOr str;
        default = null;
        description = "Wireguard public keys";
      };
    };
  };
in
{
  options.currentDevice = deviceConfig;

  options.devices = lib.mkOption {
    description = "Set of attributes describing the devices";
    type = attrsOf (submodule {
      options = deviceConfig;
    });
  };
}
