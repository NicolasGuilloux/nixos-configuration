{ pkgs, ... }:

let
  agenix = builtins.fetchTarball {
    url = "https://github.com/ryantm/agenix/archive/f6291c5935fdc4e0bef208cfc0dcab7e3f7a1c41.tar.gz";
    sha256 = "1x8nd8hvsq6mvzig122vprwigsr3z2skanig65haqswn7z7amsvg";
  };
in
{
  # https://devenv.sh/packages/
  packages = with pkgs; [
    git
    (pkgs.callPackage "${agenix}/pkgs/agenix.nix" { })
  ];

  # https://devenv.sh/languages/
  languages.nix.enable = true;

  scripts.make-iso.exec = ''
    nix build \
      --extra-experimental-features "nix-command flakes" \
      .#nixosConfigurations.NoverNixosInstaller.config.system.build.isoImage

    ISO_SIZE=$(du -sh ./result/iso/nover_nixos-x86_64-linux.iso | cut -f1)
    echo "ISO ready ($ISO_SIZE)"
  '';

  # https://devenv.sh/pre-commit-hooks/
  pre-commit.hooks.nixpkgs-fmt.enable = true;
  pre-commit.hooks.deadnix.enable = true;

  # https://devenv.sh/commands/
  enterTest = ''
    echo "Checking Nix flake"
    nix flake check \
      --extra-experimental-features "nix-command flakes" \
      --impure \
      --show-trace
  '';

  scripts.edit-age.exec = ''
    agenix -e assets/secrets/$(hostname)/$1.age
  '';
}
