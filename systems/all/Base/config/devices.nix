{ ... }:

{
  devices = {
    NoverNas = {
      localIps = [ "192.168.2.3" ];
    };

    NoverMewenn = {
      theme = "atelier-lakeside";
      pathToOsConfig = "/etc/nixos";
      isWorkstation = true;
      publicKeys = [
        "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIG7lrPthNbMa6mmqiDPCZbOZZXudGX+vBFDPYaB8ugf+ NoverMewenn"
        "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIJXh2x7ZwGHI/2yC17rRG46RemKFm2Fa+0snZ/dGlNz2 root@NoverDesktop"
        "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQDL2+V+4b0kHnzrxq1uzhJjTYMEzDDCyvO+5oq10PJ1V3TCK/zfqMjvGZKbJRb7dm6DqfGzypSkp33GQJejJjv5QkgcbYZ4nhlaQHFlBTccTgWIws6qTs6aJ+XlXcPa6Vggk+nGpku85cnOEKhqLoUNgNRWr2TkX1zTEgMwwfCT6tgo8qivMqQKPs1koVEG1kCxbkOgqd0MsjlBwgd13uZa/exgOFuj4Hh1wQehPT7uQmbRcJP3nTM6puoI9kgV30z6giUveZt/1zHTnOoyOmi2IRBuPJuXUT7hdjktCY0VcJWggxyoq6n+qcNx57qAGnaiso11Afn4/n2deyfvaiP1mJKbeia+OnPBxofvudHrwxw7wMjYVZWxDBC4QNn5SQkJoxovVon2yHdgn7CFgeTEz8rOhbwVaoKlz5EQB3mPgF2wW8rhKCkJdFQbvLq3peGgkggUbX0ngpMjB69ale2zxQn0UOOeKlqF6RU461YnXNTkfL7AKEsSeXL1CSNDTFDAauHeYBVbA0fd9UsiQNG2abHwODjIpZ6ktgLwiE75bgCeK6RL/DIZec0AlpkB7GoXnO+T9WSM+PWagOC6/V22X1sV5gmtC2z6pVicxQq3i5qTBfkqyw+a5OeK2rvzO3kq5AsnIW08gJ0N2I1ITdu9/cSf5QUlydLRjkYQefHWkQ== root@NoverDesktop"
      ];
      localIps = [ "192.168.2.4" ];
      cacheStorePublicKey = "nover-mewenn:SBt2ZuWgMggzg+WbxI/lIo2/14PY50ipl5T4fV/ebX4=";
    };

    NoverMacbook = {
      theme = "sparky";
      pathToOsConfig = "/Users/nover/.config/nix-config";
      isWorkstation = true;
      publicKeys = [
        "sk-ssh-ed25519@openssh.com AAAAGnNrLXNzaC1lZDI1NTE5QG9wZW5zc2guY29tAAAAIECkkJBGSddot0eMqqJp9e5/azo/9JjmMG+vLfxHT7DYAAAABHNzaDo= nover@NoverMacbook"
      ];
      localIps = [ "192.168.5.2" "192.168.5.3" ];
    };

    NoverNixosInstaller = {
      theme = "atelier-cave";
      pathToOsConfig = "/etc/nixos";
    };

    NoverPhone = {
      localIps = [ "192.168.5.4" ];
    };
  };
}
