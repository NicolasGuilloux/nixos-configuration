{ pkgs, ... }:

{
  # Packages
  environment.systemPackages = with pkgs; [
    git
    gitAndTools.lab
    gitAndTools.hub
    pre-commit
  ];
}
