{ pkgs, ... }:

{
  # Shells
  programs.zsh.enable = true;
  programs.fish.enable = true;

  # Extended shell programs
  programs.extendedShell = {
    eza.enable = true;
    eza.defaultParams = [ "--color-scale" "--group-directories-first" "--group" "--icons" "--header" ];
    ripgrep.enable = true;
    fd.enable = true;
    bat.enable = true;
    zoxide.enable = true;
    zoxide.enableInteractive = true;
  };

  # Aliases
  environment.shellAliases = {
    tree = "eza --color-scale -a --group --icons --header -T";
    cp = "${pkgs.xcp}/bin/xcp";

    # Git
    gco = "git checkout";
    gamend = "git commit --amend --no-edit";
    grebase = "git fetch origin && git rebase";
    greset-origin = "git reset --hard @{u}";

    # Docker
    dc = "docker compose";
    dup = "dc up -d";
    ddown = "dc down --remove-orphans";
    dr = "ddown; dup";
  };

  # Install other packages
  environment.systemPackages = with pkgs; [
    imagemagick
    unzip
    vimHugeX
    wget
    zip
    zstd
    htop
    entr
    nil
    xcp
  ];
}
