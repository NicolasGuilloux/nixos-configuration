{ config, lib, pkgs, ... }:

let
  currentDevice = config.devices."${config.networking.hostName}";
in
{
  stylix = {
    enable = true;
    image = lib.snowfall.fs.get-file "assets/images/Wallpapers/Minimal-Wallpaper-4k.jpg";
    polarity = "dark";
    opacity.terminal = 0.9;
    base16Scheme = "${pkgs.base16-schemes}/share/themes/${currentDevice.theme}.yaml";

    fonts = {
      serif = config.stylix.fonts.monospace;
      sansSerif = config.stylix.fonts.monospace;
      monospace = {
        name = "JetBrainsMono Nerd Font";
        package = pkgs.nerdfonts.override { fonts = [ "JetBrainsMono" ]; };
      };
    };
  };
}
