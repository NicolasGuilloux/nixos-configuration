{ pkgs, ... }:

{
  fonts.packages = with pkgs; [
    powerline-fonts
    noto-fonts
    (nerdfonts.override { fonts = [ "JetBrainsMono" ]; })
  ];
}

