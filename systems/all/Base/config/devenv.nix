{ pkgs, ... }:

{
  environment.systemPackages = [ pkgs.unstable.devenv ];

  programs.direnv.enable = true;
  programs.direnv.silent = true;
  programs.direnv.loadInNixShell = false;
}
