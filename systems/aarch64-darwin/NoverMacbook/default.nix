# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ lib, ... }:

{
  imports = [ ]
    ++ (lib.snowfall.fs.get-nix-files-recursive ../../all/Base)
    ++ (lib.snowfall.fs.get-non-default-nix-files-recursive ./.);

  networking.hostName = "NoverMacbook";

  # Auto upgrade nix package and the daemon service.
  services.nix-daemon.enable = true;

  # Build user
  nix.configureBuildUsers = true;
  ids.uids.nixbld = lib.mkForce 30000;

  security.pam.enableSudoTouchIdAuth = true;

  # Create /etc/zshrc that loads the nix-darwin environment.

  # Used for backwards compatibility, please read the changelog before changing.
  # $ darwin-rebuild changelog
  system.stateVersion = 4;
}

