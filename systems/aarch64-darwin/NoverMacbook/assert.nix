{ lib, ... }:

let
  isEnabled = name: predicate: {
    assertion = predicate;
    message = "${name} should be enabled.";
  };
in
{
  assertions = lib.mapAttrsToList isEnabled { };
}
