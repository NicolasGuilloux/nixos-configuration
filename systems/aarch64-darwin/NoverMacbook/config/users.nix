{ lib, pkgs, ... }:

{
  # Nover
  users.users.nover = {
    description = "Nicolas Guilloux";
    uid = 1000;
    gid = 20;
    createHome = false;
    shell = pkgs.fish;
    home = "/Users/nover";
  };

  # Home Manager configuration
  home-manager.users.nover = import (lib.snowfall.fs.get-file "homes/aarch64-darwin/nover/default.nix");
  nix.settings.trusted-users = [ "nover" ];
}
