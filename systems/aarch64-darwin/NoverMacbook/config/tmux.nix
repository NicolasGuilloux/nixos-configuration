{
  programs.tmux = {
    enableFzf = true;
    enableMouse = true;
    enableVim = true;
  };
}
