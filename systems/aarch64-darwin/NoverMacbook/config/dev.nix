{ ... }:

{
  # environment.systemPackages = with pkgs; [
  #   php82
  #   php82Packages.composer
  # ];

  homebrew.casks = [
    # "flutter"
    {
      name = "chromium";
      args.no_quarantine = true;
    }
  ];

  homebrew.brews = [
    "cocoapods"
  ];
}
