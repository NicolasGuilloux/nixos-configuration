{ config, pkgs, ... }:

let
  currentDevice = config.devices."${config.networking.hostName}";
  darwinRebuild = pkgs.writeShellScript "darwin-rebuild.sh" ''
    git -C ${currentDevice.pathToOsConfig} add ${currentDevice.pathToOsConfig}
    darwin-rebuild --flake "${currentDevice.pathToOsConfig}" "$@"
  '';

  # Ref: https://mathiasbynens.be/notes/shell-script-mac-apps
  appify = pkgs.writeShellScriptBin "appify" ''
      APPNAME=''${2:-$(basename "''${1}" '.sh')};
      DIR="''${APPNAME}.app/Contents/MacOS";

      if [ -a "''${APPNAME}.app" ]; then
        echo "''${PWD}/''${APPNAME}.app already exists :(";
        exit 1;
      fi;

      mkdir -p "''${DIR}";
      cp "''${1}" "''${DIR}/''${APPNAME}";
      chmod +x "''${DIR}/''${APPNAME}";

    echo "''${PWD}/$APPNAME.app";
  '';
in
{
  environment.systemPackages = [ appify ];

  # Aliases
  environment.shellAliases = {
    nrs = "${darwinRebuild} switch";
    nrb = "${darwinRebuild} build";
  };
}
