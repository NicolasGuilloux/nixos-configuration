{ ... }:

{
  homebrew.taps = [ "vmware-tanzu/pinniped" ];
  homebrew.brews = [ "pinniped-cli" "mkcert" "nss" ];
}
