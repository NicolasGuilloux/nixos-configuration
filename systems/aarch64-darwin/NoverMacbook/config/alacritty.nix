{ pkgs, ... }:

{
  environment.systemPackages = [ pkgs.alacritty ];

  home-manager.users.nover.programs.alacritty = {
    enable = true;
    settings = {
      # Misc
      env.TERM = "xterm-256color";

      # Window
      window = {
        dynamic_title = true;
        dynamic_padding = true;
        decorations = "full";
        # opacity = 0.9;
        padding = {
          x = 10;
          y = 10;
        };
      };

      # Scrolling
      scrolling = {
        history = 10000;
        multiplier = 3;
      };

      # Mouse
      mouse.hide_when_typing = true;

      # Font
      # font =
      #   let fontName = "JetBrainsMono Nerd Font";
      #   in {
      #     size = 12.0;
      #     normal = {
      #       family = fontName;
      #       style = "Regular";
      #     };
      #     bold = {
      #       family = fontName;
      #       style = "Bold";
      #     };
      #     italic = {
      #       family = fontName;
      #       style = "Italic";
      #     };
      #     bold_italic = {
      #       family = fontName;
      #       style = "Bold Italic";
      #     };
      #   };

      # Cursor
      cursor.style = "Block";
      cursor.unfocused_hollow = true;

      # Hints<
      hints.enabled = [
        #   {
        #     regex = "(magnet:|mailto:|gemini:|gopher:|https:|http:|news:|file:|git:|ssh:|ftp:)[^\\u0000-\\u001F\\u007F-\\u009F<>\\\"\\\\\\s{-}\\\\^⟨⟩`]+";
        #     command = "open";
        #     post_processing = true;
        #     mouse = {
        #       enabled = true;
        #       mods = "Command";
        #     };
        #     binding = {
        #       key = "U";
        #       mods = "Control|Shift";
        #     };
        #   }
        {
          command = "open";
          hyperlinks = true;
          mouse = {
            enabled = true;
            mods = "Command";
          };
          binding = {
            key = "U";
            mods = "Control|Shift";
          };
        }
      ];

      # nord theme
      # colors = {
      #   draw_bold_text_with_bright_colors = true;
      #   primary = {
      #     background = "0x1a1b26";
      #     foreground = "0xc0caf5";
      #   };
      #   normal = {
      #     black = "0x15161e";
      #     red = "0xf7768e";
      #     green = "0x9ece6a";
      #     yellow = "0xe0af68";
      #     blue = "0x7aa2f7";
      #     magenta = "0xbb9af7";
      #     cyan = "0x7dcfff";
      #     white = "0xa9b1d6";
      #   };
      #   bright = {
      #     black = "0x414868";
      #     red = "0xf7768e";
      #     green = "0x9ece6a";
      #     yellow = "0xe0af68";
      #     blue = "0x7aa2f7";
      #     magenta = "0xbb9af7";
      #     cyan = "0x7dcfff";
      #     white = "0xc0caf5";
      #   };
      #   indexed_colors = [
      #     {
      #       index = 16;
      #       color = "0xff9e64";
      #     }
      #     {
      #       index = 17;
      #       color = "0xdb4b4b";
      #     }
      #   ];
      # };
    };
  };
}
