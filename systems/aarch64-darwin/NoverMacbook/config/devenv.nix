{ pkgs, ... }:

{
  environment.systemPackages = [ pkgs.unstable.devenv ];

  # Direnv support
  programs.direnv.enable = true;
  programs.direnv.silent = true;
  programs.direnv.loadInNixShell = false;
}
