{ pkgs, ... }:

{
  environment.systemPackages = with pkgs; [ unstable.virt-manager ];
  environment.variables.GSETTINGS_BACKEND = "keyfile";
}
