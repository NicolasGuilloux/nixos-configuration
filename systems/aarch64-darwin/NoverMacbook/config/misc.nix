{ ... }:

{
  # Brews
  homebrew.brews = [
    "borders"
    "starship"
    "tailscale"
    "openssh"
    "volta"
    "qt"
    "libtool"
    "cue"
    "m4"
  ];

  # Casks
  homebrew.casks = [
    "aldente"
    "alt-tab"
    "audacity"
    "appcleaner"
    "balenaetcher"
    "command-x"
    "displaylink"
    "firefox"
    "hiddenbar"
    "inkscape"
    "latest"
    "macfuse"
    "meetingbar"
    "microsoft-remote-desktop"
    "moonlight"
    "mqttx"
    "notion"
    "pliim"
    "postman"
    "proton-drive"
    "protonmail-bridge"
    "protonvpn"
    "raspberry-pi-imager"
    "sidequest"
    "signal"
    "steam"
    "the-unarchiver"
    "transmission"
    "upscayl"
    "utm"
    "visual-studio-code"
    "vlc"
    "zed"
    "xquartz"
  ];

  # Taps
  homebrew.taps = [
    # default
    "homebrew/bundle"
    "homebrew/cask-fonts"
    "homebrew/services"

    # custom
    "FelixKratz/formulae" # borders
    "cue-lang/tap"
  ];
}
