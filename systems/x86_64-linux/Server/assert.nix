{ config, lib, ... }:

let
  isEnabled = name: predicate: {
    assertion = predicate;
    message = "${name} should be enabled.";
  };
in
{
  assertions = lib.mapAttrsToList isEnabled {
    "Fail2Ban" = config.services.fail2ban.enable;
    "Traefik" = config.services.traefik.enable;
  };
}
