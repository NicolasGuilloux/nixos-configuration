{ config, lib, ... }:

{
  options.virtualisation.oci-containers.containers = lib.mkOption {
    type = lib.types.attrsOf (lib.types.submodule {
      config.labels."com.centurylinklabs.watchtower.enable" = lib.mkDefault "true";
    });
  };
}
