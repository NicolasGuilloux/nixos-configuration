{ config, ... }:

{
  # Traefik configuration
  services.traefik.enable = true;
  services.traefik.enableDocker = config.virtualisation.docker.enable;
  services.traefik.environmentFiles = [ config.age.secrets.traefik-env.path ];

  # Firewall ports
  networking.firewall.allowedTCPPorts = if config.services.traefik.enable then [ 80 443 ] else [ ];
}
