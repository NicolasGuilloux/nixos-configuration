{ config, ... }:

let
  socket =
    if config.virtualisation.docker.enable then
      "/var/run/docker.sock"
    else
      "/var/run/podman/podman.sock";


in
{
  virtualisation.oci-containers.containers.watchtower = {
    autoStart = true;
    image = "containrrr/watchtower";
    volumes = [
      "/etc/localtime:/etc/localtime:ro"
      "${socket}:/var/run/docker.sock:ro"
      "/root/.docker/config.json:/config.json:ro"
    ];
    environment = {
      WATCHTOWER_CLEANUP = "true";
      WATCHTOWER_POLL_INTERVAL = "3600";
      WATCHTOWER_LABEL_ENABLE = "true";
      WATCHTOWER_DEBUG = "true";
    };
  };
}
