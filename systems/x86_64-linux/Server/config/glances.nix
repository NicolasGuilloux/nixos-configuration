{ pkgs, ... }:

{
  systemd.services.glances = {
    enable = true;
    description = "Start Glances monitoring service over HTTP";
    wantedBy = [ "multi-user.target" ];
    script = ''
      ${pkgs.glances}/bin/glances -w --disable-webui
    '';
  };
}
