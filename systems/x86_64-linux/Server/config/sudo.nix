{ ... }:

{
  security.sudo.extraRules = [
    {
      users = [ "nover" ];
      commands = [
        {
          command = "ALL";
          options = [ "NOPASSWD" ];
        }
      ];
    }
  ];
}
