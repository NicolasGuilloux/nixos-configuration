{ config, pkgs, ... }:

{
  services.cockpit.enable = true;
  services.cockpit.package = pkgs.unstable.cockpit;
  services.cockpit.openFirewall = true;
  services.cockpit.settings.WebService.AllowUnencrypted = true;

  environment.systemPackages = with pkgs; [
    # cockpit-apps.podman-containers
    nover.cockpit-machines
    libvirt # needed for virtual-machines
    virt-manager # needed for virtual-machines
  ];

  users.users.nover.extraGroups = [ "qemu-libvirtd" ];

  # Dashboard
  services.homer.categories.Autres.services.Cockpit = {
    subtitle = "Managing server";
    url = "http://192.168.2.4:${toString config.services.cockpit.port}/";
    logo = "https://pbs.twimg.com/profile_images/1372234268345167876/09jqzQq4_400x400.png";
    tag = "local";
  };
}
