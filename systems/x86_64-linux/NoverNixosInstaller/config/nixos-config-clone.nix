{ lib, ... }:

let
  configFolder = lib.snowfall.fs.get-file "/";
in
{
  installer.cloneConfig = false;
  boot.postBootCommands = lib.mkAfter ''
    rm -R /etc/nixos
    ln -f -s ${configFolder} /etc/nixos
  '';
}
