# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ lib, modulesPath, ... }:

{
  imports = [ (modulesPath + "/installer/cd-dvd/installation-cd-minimal.nix") ]
    ++ (lib.snowfall.fs.get-nix-files-recursive ../../all/Base)
    ++ (lib.snowfall.fs.get-nix-files-recursive ../Base)
    ++ (lib.snowfall.fs.get-nix-files-recursive ./config)
    ++ (lib.snowfall.fs.get-non-default-nix-files ./.);
}
