{ config, lib, pkgs, ... }:

{
  isoImage.isoName = lib.mkForce "${config.isoImage.isoBaseName}-${pkgs.stdenv.hostPlatform.system}.iso";
  isoImage.isoBaseName = "nover_nixos";
  isoImage.makeEfiBootable = true;
  isoImage.makeUsbBootable = true;
}
