{ lib, ... }:

{
  # Enable the OpenSSH daemon.
  services.openssh = {
    enable = lib.mkDefault true;
    settings.PasswordAuthentication = lib.mkDefault true;
    settings.X11Forwarding = true;
    openFirewall = true;
    ports = [ 22 ];
    extraConfig = ''
      ClientAliveInterval 3600
    '';
  };
}
