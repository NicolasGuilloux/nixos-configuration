{ config, ... }:

let
  colors = {
    directory = { fg = "#000000"; bg = "#BF616A"; };
    git = { fg = "#000000"; bg = "#EBCB8B"; };
    status = { bg = "#9A348E"; };
    versions = { bg = "#3B4252"; };
    docker = { bg = "#06969A"; };
    time = { bg = "#33658A"; };
  };

  formatColors = input: builtins.concatStringsSep " " [
    (if (builtins.hasAttr "fg" input) then "fg:${input.fg}" else "")
    (if (builtins.hasAttr "bg" input) then "bg:${input.bg}" else "")
  ];

  #### Improvement
in
{
  # Starship
  programs.starship.enable = true;
  programs.starship.settings = {
    # Left part
    format = builtins.concatStringsSep "" [
      "[](${formatColors { fg = colors.directory.bg; }})"
      "$directory" /*  */
      "[](${formatColors { fg = colors.directory.bg; bg = colors.git.bg; }})"
      "$git_branch"
      "$git_status"
      "[ ](${formatColors { fg = colors.git.bg; }})"
    ];

    # Right part
    right_format = builtins.concatStringsSep "" [
      "[](${formatColors { fg = colors.status.bg; }})"
      "$status"
      "$shell"
      "$username"
      "[${config.networking.hostName} ](${formatColors colors.status})"
      "[](${formatColors { fg = colors.status.bg; bg = colors.versions.bg; }})"
      "$c"
      "$elixir"
      "$elm"
      "$golang"
      "$haskell"
      "$julia"
      "$nodejs"
      "$nim"
      "$nix_shell"
      "$php"
      "$rust"
      "[](${formatColors { fg = colors.versions.bg; bg = colors.docker.bg; }})"
      "$docker_context"
      "[](${formatColors { fg = colors.docker.bg; bg = colors.time.bg; }})"
      "$time"
      "[ ](${formatColors { fg = colors.time.bg; }})"
    ];

    add_newline = true;

    username = {
      show_always = false;
      style_user = formatColors colors.status;
      style_root = formatColors colors.status;
      format = "[$user ]($style)";
    };

    directory = {
      style = formatColors colors.directory;
      format = "[ $path ]($style)";
      truncation_length = 3;
      truncate_to_repo = false;
      truncation_symbol = "…/";
      substitutions = {
        "~/Documents" = "📄 ";
        "~/Téléchargements" = "📥 ";
        "~/Musique" = "🎶🎼 ";
        "~/Images" = "🖼️ ";
        "~/Projets/Carrefour" = "🇨 ";
      };
    };

    c = {
      symbol = "";
      style = formatColors colors.versions;
      format = "[ $symbol ($version) ]($style)";
    };

    docker_context = {
      symbol = "";
      style = formatColors colors.docker;
      format = "[ $symbol $context ]($style) $path";
    };

    elixir = {
      symbol = "";
      style = formatColors colors.versions;
      format = "[ $symbol ($version) ]($style)";
    };

    elm = {
      symbol = "";
      style = formatColors colors.versions;
      format = "[ $symbol ($version) ]($style)";
    };

    git_branch = {
      symbol = "";
      style = formatColors colors.git;
      format = "[ $symbol $branch ]($style)";
    };

    git_status = {
      style = formatColors colors.git;
      diverged = "🖖 ";
      stashed = "📦 ";
      modified = "✏️ ";
      format = "[$all_status$ahead_behind]($style)";
    };

    golang = {
      symbol = "";
      style = formatColors colors.versions;
      format = "[ $symbol ($version) ]($style)";
    };

    haskell = {
      symbol = "";
      style = formatColors colors.versions;
      format = "[ $symbol ($version) ]($style)";
    };

    nodejs = {
      symbol = "";
      style = formatColors colors.versions;
      format = "[ $symbol ($version) ]($style)";
    };

    nim = {
      symbol = "";
      style = formatColors colors.versions;
      format = "[ $symbol ($version) ]($style)";
    };

    nix_shell = {
      symbol = "❄";
      style = formatColors colors.versions;
      format = "[ $symbol ($version) ]($style)";
    };

    php = {
      symbol = "🐘";
      style = formatColors colors.versions;
      format = "[ $symbol ($version) ]($style)";
    };

    rust = {
      symbol = "";
      style = formatColors colors.versions;
      format = "[ $symbol ($version) ]($style)";
    };

    status = {
      disabled = false;
      style = formatColors colors.status;
      format = "[$symbol$status ]($style)";
    };

    shell = {
      disabled = false;
      style = formatColors colors.status;
      format = "[$indicator ]($style)";
      bash_indicator = " ";
      fish_indicator = "🐠 ";
      zsh_indicator = "zsh ";
    };

    time = {
      disabled = false;
      time_format = "%R";
      style = formatColors colors.time;
      format = "[ ♥ $time ]($style)";
    };
  };
}
