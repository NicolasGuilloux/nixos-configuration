{ lib, ... }:

{
  # ACME Challenge
  security.acme.acceptTerms = true;
  security.acme.defaults.email = "nicolas.guilloux@proton.me";

  # Use recommended settings
  services.nginx.recommendedGzipSettings = lib.mkDefault true;
  services.nginx.recommendedOptimisation = lib.mkDefault true;
  services.nginx.recommendedProxySettings = lib.mkDefault true;
  services.nginx.recommendedTlsSettings = lib.mkDefault true;
}
