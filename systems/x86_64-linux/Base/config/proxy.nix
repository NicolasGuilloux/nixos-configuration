{ config, ... }:

{
  networking.proxy.noProxy = builtins.concatStringsSep "," (
    [ "127.0.0.1" "localhost" ".local" ".localdomain" ]
    ++ config.networking.hosts."127.0.0.1"
  );
}
