{ config, ... }:

{
  services.zfs.trim.enable = true;

  services.zfs.autoSnapshot = {
    enable = true;
    frequent = 8;
    monthly = 3;
  };

  services.zfs.autoScrub = {
    enable = (config.services.zfs.autoScrub.pools != [ ]);
    interval = "daily";
  };
}
