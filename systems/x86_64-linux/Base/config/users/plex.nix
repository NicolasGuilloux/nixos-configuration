{ config, lib, ... }:

{
  users.users.plex = lib.mkIf config.services.plex.enable {
    home = "${config.services.plex.dataDir}/home";
  };
}
