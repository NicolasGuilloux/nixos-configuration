{ config, ... }:

{
  # User configuration
  users.users.root.shell = config.users.users.nover.shell;
}
