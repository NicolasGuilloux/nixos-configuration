{ config, lib, ... }:

{
  # User configuration
  users.users.nover = {
    description = "Nicolas Guilloux";
    uid = 1000;
    isNormalUser = true;
    createHome = true;
    hashedPassword = "$y$j9T$UbRpbNEtUbzMj96ck/CMA/$TMCAz/4xQyxC31UoPud.UJO3ZPOefB.2wJsXwAMbWlA";
    shell = config.users.defaultUserShell;
    group = "users";
    openssh.authorizedKeys.keys = lib.mkMerge (lib.mapAttrsToList (_name: value: value.publicKeys) config.devices);
    extraGroups = [
      "docker"
      "podman"
      "input"
      "networkmanager"
      "nover"
      "power"
      "wheel"
      "libvirtd"
      "davfs2"
      "media"
      "render"
      "video"
    ];
  };

  # Nix privileges
  nix.settings.trusted-users = [ "nover" ];
}
