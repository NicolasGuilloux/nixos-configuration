{ config, lib, ... }:

{
  age.folders = [
    (lib.snowfall.fs.get-file "assets/secrets/${config.networking.hostName}")
  ];
}
