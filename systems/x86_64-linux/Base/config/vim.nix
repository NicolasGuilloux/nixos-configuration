{ pkgs, ... }:

let
  vimConfig = {
    enable = true;
    settings.tabstop = 4;
    settings.smartcase = true;
    plugins = with pkgs.vimPlugins; [
      awesome-vim-colorschemes
      elm-vim
      neocomplete-vim
      rainbow_parentheses-vim
      # vim-addon-syntax-checker
      vim-closetag
      vim-json
      vim-markdown
      vim-multiple-cursors
    ];
  };
in
{
  # Install other packages
  environment.systemPackages = with pkgs; [ vimHugeX neovim ];

  # Environment variables
  environment.sessionVariables = {
    EDITOR = "vim";
    VISUAL = "vim";
  };

  # Configuration
  home-manager.users.nover.programs.vim = vimConfig;
  home-manager.users.root.programs.vim = vimConfig;
}

