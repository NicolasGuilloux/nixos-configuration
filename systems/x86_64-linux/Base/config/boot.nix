{ config, lib, pkgs, ... }:

with lib;

{
  # General boot options
  boot = {
    tmp.cleanOnBoot = true;
    kernelPackages = pkgs.linuxPackages;
    crashDump.enable = true;
    supportedFilesystems = [ "ntfs" ];

    # Global configuration
    loader.timeout = mkDefault 1;
    loader.efi.canTouchEfiVariables = mkDefault false;

    # Configure Grub
    loader.grub = {
      enable = mkDefault false;
      configurationLimit = mkDefault 15;
      device = mkDefault "nodev";
      efiSupport = mkDefault true;
      splashImage = lib.snowfall.fs.get-file "assets/images/Wallpapers/grub.jpg";
    };

    # Configure Systemd boot
    loader.systemd-boot = {
      enable = mkDefault true;
      configurationLimit = mkDefault config.boot.loader.grub.configurationLimit;
    };

    # Enable Plymouth
    plymouth.enable = mkDefault false;
  };
}
