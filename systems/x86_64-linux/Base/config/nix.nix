{ config, lib, pkgs, ... }:

{
  # Automatic garbage collect
  nix.gc = {
    automatic = true;
    dates = "12:50";
    options = "--delete-older-than 15d";
  };

  # Automatic cleanup
  nix.settings.auto-optimise-store = true;
  nix.optimise = {
    automatic = true;
    dates = [ "13:20" ];
  };

  # List packages installed in system profile.
  environment.systemPackages = with pkgs; [ nixpkgs-fmt nox ];

  # Experimental
  nix.settings.experimental-features = [ "nix-command" "flakes" ];

  # Library fixes
  programs.nix-ld.enable = true;
  programs.nix-ld.libraries = with pkgs; [
    stdenv.cc.cc
    glibc.out
    glibc
  ];

  # Beautiful diff
  system.activationScripts.diff = {
    supportsDryActivation = true;
    text = ''
      ${pkgs.nvd}/bin/nvd --nix-bin-dir=${pkgs.nix}/bin diff /run/current-system "$systemConfig"
    '';
  };

  # Serve store over SSH
  nix.sshServe.enable = true;
  nix.sshServe.keys = lib.mkMerge (lib.mapAttrsToList (_name: value: value.publicKeys) config.devices);
}
