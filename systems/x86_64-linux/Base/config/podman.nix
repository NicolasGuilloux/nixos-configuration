{ ... }:

{
  virtualisation.podman.enable = true;
  virtualisation.podman.autoPrune.enable = true;
  virtualisation.podman.dockerCompat = true;
  virtualisation.podman.defaultNetwork.settings.dns_enabled = true;

  # OCI Containers
  virtualisation.oci-containers.backend = "podman";

  # Disable Docker
  virtualisation.docker.enable = false;
}
