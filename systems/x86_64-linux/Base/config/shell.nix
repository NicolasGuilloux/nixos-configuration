{ config, pkgs, ... }:

let
  currentDevice = config.devices."${config.networking.hostName}";

  nixosRebuild = pkgs.writeShellScript "nixos-rebuild.sh" ''
    git -C ${currentDevice.pathToOsConfig} add ${currentDevice.pathToOsConfig}
    sudo nixos-rebuild --flake "${currentDevice.pathToOsConfig}" "$@"
  '';
in
{
  # Install other packages
  environment.systemPackages = with pkgs; [
    pciutils
    usbutils
    inetutils
    ndisc6
    gimp
    jq
  ];

  # ZSH config
  programs.zsh = {
    autosuggestions.enable = true;
    syntaxHighlighting.enable = true;
    histSize = 9999;
    promptInit = "ZLE_RPROMPT_INDENT=0";

    ohMyZsh = {
      enable = true;
      plugins = [ "git" "sudo" ];
    };
  };

  # Make Fish the default shell
  users.defaultUserShell = pkgs.fish;

  # Aliases
  environment.shellAliases = {
    nrs = "${nixosRebuild} switch";
    nrb = "${nixosRebuild} build";
    ncg = "sudo nix-collect-garbage -d";
    journal = "sudo journalctl -xeu";
    ujournal = "journalctl --user -xeu";
  };
}

