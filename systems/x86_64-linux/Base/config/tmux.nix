{ pkgs, ... }:

{
  programs.tmux = {
    clock24 = true;
    newSession = true;
    keyMode = "vi";
    extraConfig = ''
      set -g base-index 1
    '';
    plugins = with pkgs.tmuxPlugins; [
      resurrect
    ];
  };
}
