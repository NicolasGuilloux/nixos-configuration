{
  # Add cachix
  nix.settings = {
    trusted-public-keys = [
      "numtide.cachix.org-1:2ps1kLBUWjxIneOy1Ik6cQjb41X0iXVXeHigGmycPPE="
    ];
    substituters = [
      "https://numtide.cachix.org"
    ];
  };
}
