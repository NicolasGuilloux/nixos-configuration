{ config, lib, ... }:

let
  sshDevices = lib.filterAttrs
    (_name: device: device.publicKeys != [ ])
    config.devices;
in
{
  programs.ssh.forwardX11 = true;
  programs.ssh.setXAuthLocation = true;

  # SSH public informations
  programs.ssh.knownHosts =
    lib.mapAttrs
      (name: device: {
        publicKey = lib.head device.publicKeys;
        hostNames = [ name ] ++ device.localIps;
      })
      sshDevices;
}
