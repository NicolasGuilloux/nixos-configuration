{ lib, ... }:

{
  # Select internationalisation properties.
  i18n.defaultLocale = "fr_FR.UTF-8";

  # New style
  console.keyMap = "fr";
  console.font = lib.mkDefault "Lat2-Terminus16";

  # Set your time zone.
  time.timeZone = "Europe/Paris";

  # Systemd conf
  systemd.extraConfig = lib.mkDefault "DefaultTimeoutStopSec=10s";

  # Enable DConf compatibility
  programs.dconf.enable = lib.mkDefault true;

  # Enable firewall
  networking.firewall.enable = lib.mkDefault true;
}
