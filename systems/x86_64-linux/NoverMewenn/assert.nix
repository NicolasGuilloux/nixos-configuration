{ config, lib, ... }:

let
  isEnabled = name: predicate: {
    assertion = predicate;
    message = "${name} should be enabled.";
  };
in
{
  assertions = lib.mapAttrsToList isEnabled {
    "Libvirtd" = config.virtualisation.libvirtd.enable;
    "Home Assistant Remote" = config.services.home-assistant-remote.enable;
    "Traefik" = config.services.traefik.enable;
    "Plex" = config.services.plex.enable;
    "Prowlarr" = config.services.prowlarr.enable;
    "Radarr" = config.services.radarr.enable;
    "Sonarr" = config.services.sonarr.enable;
    # "Primelooter" = config.virtualisation.oci-containers.containers.primelooter.autoStart;
  };
}
