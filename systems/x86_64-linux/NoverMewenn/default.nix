# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ lib, ... }:

{
  imports = [ ]
    ++ (lib.snowfall.fs.get-nix-files-recursive ../../all/Base)
    ++ (lib.snowfall.fs.get-nix-files-recursive ../Base)
    ++ (lib.snowfall.fs.get-nix-files-recursive ../Server)
    ++ (lib.snowfall.fs.get-nix-files-recursive ./config)
    ++ (lib.snowfall.fs.get-non-default-nix-files ./.);

  # Hostname
  networking.mainSubdomain = "mewenn";
  networking.hostName = "NoverMewenn";

  # Boot
  boot.loader.grub.enable = false;
  boot.loader.systemd-boot.enable = true;
  boot.kernel.sysctl."net.ipv4.igmp_max_memberships" = 50;

  # Network
  networking.useDHCP = true;
  networking.enableIPv6 = true;

  # ZFS
  networking.hostId = "981a0a34";
  services.zfs.autoScrub.pools = [ "zfs-pool" ];

  # Misc
  powerManagement.enable = false;

  # This value determines the NixOS release with which your system is to be
  # compatible, in order to avoid breaking some software such as database
  # servers. You should change this only after NixOS release notes say you
  # should.
  system.stateVersion = lib.mkDefault "21.05"; # Did you read the comment?
}
