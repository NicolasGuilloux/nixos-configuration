{ lib, ... }:

with lib;
with types;

{
  options.device.gaming = mkEnableOption "Enable Steam big picture and Sunshine daemon";
  options.device.ai = mkEnableOption "Enable AI";
}
