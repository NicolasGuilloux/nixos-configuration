{ ... }:

{
  hardware.cpu.amd.updateMicrocode = true;
  virtualisation.libvirtd.IOMMUType = "amd";
}
