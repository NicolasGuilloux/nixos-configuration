{ pkgs, ... }:

{
  services.cloudflared.enable = true;
  services.cloudflared.package = pkgs.unstable.cloudflared;
}
