{ config, lib, pkgs, ... }:

{
  security.wrappers = {
    mount.source = "${pkgs.utillinux}/bin/mount";
    umount.source = "${pkgs.utillinux}/bin/umount";
  };

  # Allow Other User
  programs.fuse.userAllowOther = true;

  # Declare SSHFS
  system.fsPackages = [ pkgs.sshfs-fuse ];

  systemd.mounts = [
    {
      what = "Nover@${lib.head config.devices.NoverNas.localIps}:/Multimedia";
      where = "/mount/Multimedia";
      type = "fuse.sshfs";
      wantedBy = [ "multi-user.target" ];
      options = builtins.concatStringsSep "," [
        "noauto"
        "_netdev"
        "users"
        "idmap=user"
        "transform_symlinks"
        "uid=1000"
        "gid=${toString config.users.groups.media.gid}"
        "allow_other"
        "reconnect"
        "IdentityFile=/home/nover/.ssh/id_ed25519"
      ];
    }
    {
      what = "Nover@${lib.head config.devices.NoverNas.localIps}:/Jeux";
      where = "/mount/Jeux";
      type = "fuse.sshfs";
      wantedBy = [ "multi-user.target" ];
      options = builtins.concatStringsSep "," [
        "noauto"
        "_netdev"
        "users"
        "idmap=user"
        "transform_symlinks"
        "uid=1000"
        "allow_other"
        "reconnect"
        "IdentityFile=/home/nover/.ssh/id_ed25519"
      ];
    }
  ];

}
