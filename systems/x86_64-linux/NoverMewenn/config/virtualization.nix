{ config, pkgs, ... }:

{
  # Global
  virtualisation.libvirtd.enable = true;
  virtualisation.libvirtd.qemu.swtpm.enable = true;
  virtualisation.libvirtd.qemu.ovmf.enable = true;
  virtualisation.libvirtd.onBoot = "ignore";
  virtualisation.spiceUSBRedirection.enable = true;

  # Misc
  programs.dconf.enable = true;
  security.polkit.enable = true;

  # Add the various packages 
  environment.systemPackages = with pkgs; [
    virt-manager
    dconf
    libguestfs-with-appliance
    swtpm
    nover.iommu-groups
    nover.cpu-manipulation
    virtiofsd
  ];

  networking.firewall.checkReversePath = false;

  # Creates the SecureBoot files
  environment.etc = {
    "ovmf/edk2-x86_64-secure-code.fd" = {
      source = config.virtualisation.libvirtd.qemu.package + "/share/qemu/edk2-x86_64-secure-code.fd";
    };

    "ovmf/edk2-i386-vars.fd" = {
      source = config.virtualisation.libvirtd.qemu.package + "/share/qemu/edk2-i386-vars.fd";
      mode = "0644";
      user = "libvirtd";
    };
  };

  # Cf https://github.com/NixOS/nixpkgs/issues/198267
  networking.bridges.br0.interfaces = [ "enp6s0" ];
  networking.interfaces.br0.useDHCP = true;
  networking.dhcpcd.extraConfig = ''
    interface br0

  '';

  # networking.interfaces.virbr1 = {
  #   virtual = true;
  #   useDHCP = false;
  #   ipv4 = {
  #     addresses = [
  #       {
  #         address = "192.168.6.1";
  #         prefixLength = 24;
  #       }
  #     ];
  #   };
  # };

  networking.vlans.vlan6 = {
    id = 6;
    interface = "br0";
  };
}
