{ ... }:

{
  # SSO default config
  services.reverseProxies.default.sso.enable = true;
  services.reverseProxies.default.sso.provider = "authentik";

  # Cloudflare certificate resolver
  services.traefik.staticConfigOptions.certificatesResolvers = {
    cloudflare = {
      acme = {
        email = "nicolas.guilloux@proton.me";
        storage = "/var/lib/traefik/acme-cloudflare.json";
        dnsChallenge = {
          provider = "cloudflare";
          resolvers = [ "1.1.1.1:53" "1.0.0.1:53" ];
        };
      };
    };
  };

  services.traefik.group = "podman";
  services.traefik.staticConfigOptions.providers.docker = {
    endpoint = "unix:///var/run/podman/podman.sock";
    watch = true;
    exposedByDefault = false;
  };

  # Dashboard
  # services.traefik.staticConfigOptions.api.insecure = true;
  # networking.firewall.allowedTCPPorts = [ 8080 ];

  services.traefik.dynamicConfigOptions.http.routers.dashboard_ext = {
    rule = "Host(`traefik.nicolasguilloux.eu`)";
    service = "api@internal";
    tls.certResolver = "cloudflare";
    entryPoints = [ "websecure" ];
    middlewares = [ "authentik" ];
  };

  # Dashboard
  services.homer.categories.Autres.services.Traefik = {
    subtitle = "Managing requests";
    url = "https://traefik.nicolasguilloux.eu";
    logo = "https://upload.wikimedia.org/wikipedia/commons/1/1b/Traefik.logo.png";
  };
}
