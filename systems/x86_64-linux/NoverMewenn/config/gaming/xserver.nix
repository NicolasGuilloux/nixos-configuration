{ config, lib, ... }:

{
  config = lib.mkIf config.device.gaming {
    services.xserver = {
      enable = true;
      autorun = false;

      desktopManager.xfce.enable = true;
      desktopManager.xfce.enableScreensaver = false;
      displayManager.lightdm.enable = true;

      virtualScreen = {
        x = 1920;
        y = 1080;
      };
    };

    services.displayManager.autoLogin.enable = true;
    services.displayManager.autoLogin.user = "nover";

    hardware.pulseaudio.enable = config.device.gaming;
  };
}
