{ config, lib, pkgs, ... }:

{
  services.sunshine = lib.mkIf config.device.gaming {
    enable = true;
    package = pkgs.sunshine.override { cudaSupport = true; };
    autoStart = false;
    openFirewall = true;
    capSysAdmin = true;
  };
}
