{ config, ... }:

{
  programs.steam.enable = config.device.gaming;
}
