{ ... }:

{
  boot.kernel.sysctl = {
    "net.core.wmem_max" = 16777216;
    "net.core.wmem_default" = 131072;
    "net.core.rmem_max" = 16777216;
    "net.core.rmem_default" = 131072;
  };
}
