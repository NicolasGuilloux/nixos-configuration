{ config, ... }:

{
  services.homer.enable = true;
  services.homer.port = 8085;
  services.homer.config = {
    title = "Dashboard";
    subtitle = "Nover";
    logo = "logo.png";
    header = true;
    footer = false;
    theme = "default";
    connectivityCheck = true;
  };

  services.homer.categories = {
    Domotique = {
      icon = "fas fa-home";
      order = 2;
    };
    Multimedia = {
      icon = "fas fa-film";
      order = 1;
    };
    Autres = {
      icon = "fas fa-gear";
      order = 3;
      services = {
        "Synology NAS" = {
          subtitle = "Mass storage";
          url = "https://192.168.2.3:5001/";
          logo = "https://play-lh.googleusercontent.com/HjbYWbXJ-6e6Cia-mBbHDSdontW1yE6MHMaXqlHW80CQegDOEPQ1HGACxvEpnqCUHgo";
          tag = "local";
        };

        Network = {
          subtitle = "Network management";
          url = "https://192.168.5.1/network/default/dashboard";
          logo = "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQWXOfgWfaEUA-cVpRE3i3cvwSlNrqCkTqYBh0pRgFFSg&s";
          tag = "local";
        };

        Protect = {
          subtitle = "Video surveillance";
          url = "https://192.168.5.1/protect/dashboard";
          logo = "https://cdn6.aptoide.com/imgs/d/4/c/d4c8e26f2bdb2244a3bf8813905796ba_icon.png";
          tag = "local";
        };
      };
    };
  };

  # Reverse proxy
  services.reverseProxies.services.dash.port = config.services.homer.port;
  services.reverseProxies.services.dash.provider = "cloudflare";
}
