{ config, ... }:

{
  services.gitlab-runner.enable = true;
  services.gitlab-runner.services.docker = {
    description = "A generic docker runner";
    executor = "docker";
    dockerImage = "nixos/nix";
    authenticationTokenConfigFile = config.age.secrets.gitlab-runner-env.path;
  };
}
