{ config, lib, ... }:

let
  cfg = {
    dataDir = "/var/lib/authentik";
    port = 9001;
  };

  baseConfig = {
    image = "ghcr.io/goauthentik/server";
    volumes = [
      "${cfg.dataDir}/media:/media"
      "${cfg.dataDir}/templates:/templates"
      "${cfg.dataDir}/certs:/certs"
    ];
    extraOptions = [ "--pod=authentik" ];
    dependsOn = [ "authentik-redis" "authentik-db" ];
    environmentFiles = [ config.age.secrets.authentik-env.path ];
    environment = {
      AUTHENTIK_LISTEN__HTTP = "0.0.0.0:${toString cfg.port}";
      AUTHENTIK_REDIS__HOST = "authentik-redis";
      AUTHENTIK_POSTGRESQL__HOST = "authentik-db";
      AUTHENTIK_POSTGRESQL__USER = "authentik";
      AUTHENTIK_POSTGRESQL__NAME = "authentik";
      AUTHENTIK_POSTGRESQL__PASSWORD = "authentik";
    };
  };
in
{
  virtualisation.oci-containers.containers.authentik-redis = {
    image = "docker.io/library/redis";
    volumes = [
      "${cfg.dataDir}/redis:/data"
    ];
    extraOptions = [ "--pod=authentik" ];
  };

  virtualisation.oci-containers.containers.authentik-db = {
    image = "docker.io/library/postgres:16";
    volumes = [
      "${cfg.dataDir}/database:/var/lib/postgresql/data"
    ];
    dependsOn = [ "authentik-redis" ];
    extraOptions = [ "--pod=authentik" ];
    environment = {
      POSTGRES_DB = "authentik";
      POSTGRES_USER = "authentik";
      POSTGRES_PASSWORD = "authentik";
    };
  };

  virtualisation.oci-containers.containers.authentik = baseConfig // {
    cmd = [ "server" ];
    dependsOn = [ "authentik-worker" ];
  };

  virtualisation.oci-containers.containers.authentik-worker = baseConfig // {
    cmd = [ "worker" ];
  };

  systemd.services.podman-authentik-redis.preStart = lib.mkAfter ''
    podman pod create \
      --replace \
      -p 127.0.0.1:${toString cfg.port}:${toString cfg.port} \
      authentik
  '';

  networking.firewall.allowedTCPPorts = [ cfg.port ];

  # Reverse proxy
  services.reverseProxies.services.auth.oci-container.name = "authentik";
  services.reverseProxies.services.auth.oci-container.port = cfg.port;
  services.reverseProxies.services.auth.provider = "cloudflare";
  services.reverseProxies.services.auth.sso.enable = false;

  # Dashboard
  services.homer.categories.Autres.services.Authentik = {
    subtitle = "SSO";
    url = "https://auth.nicolasguilloux.eu";
    logo = "https://cf.appdrag.com/dashboard-openvm-clo-b2d42c/uploads/goauthentik-LRFF.png";
  };
}
