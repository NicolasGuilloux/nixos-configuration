{ config, lib, ... }:

{
  virtualisation.oci-containers.containers.fgc-alex = {
    image = "ghcr.io/vogler/free-games-claimer";
    # ports = [ "6080:6080" ];
    volumes = [ "/var/lib/fgc-alex:/fgc/data" ];
    environmentFiles = [ config.age.secrets.fgc-alex-env.path ];
  };

  # Restarts only on failure
  systemd.services.podman-fgc-alex.serviceConfig.Restart = lib.mkForce "on-failure";

  # Cloudflare
  services.reverseProxies.services."fgc.alex".provider = "cloudflare";
  services.reverseProxies.services."fgc.alex".oci-container.name = "fgc-alex";
  services.reverseProxies.services."fgc.alex".oci-container.port = 6080;

  # Start it every night at 4am
  services.cron = {
    enable = true;
    systemCronJobs = [ "0 4 * * *     root     systemctl restart podman-fgc.service" ];
  };
}
