{ config, lib, ... }:

let
  cfg = {
    dataDir = "/var/lib/local-ai";
    port = 8081;
    openFirewall = true;
  };
in
{
  config = lib.mkIf config.device.ai {
    virtualisation.oci-containers.containers.local-ai = {
      image = "localai/localai:latest-aio-gpu-nvidia-cuda-12";
      extraOptions = [ "--gpus" "all" ];
      ports = [ "${toString cfg.port}:8080" ];
      volumes = [ "${cfg.dataDir}/models:/build/models" ];
    };

    # Firewall
    networking.firewall.allowedTCPPorts = if cfg.openFirewall then [ cfg.port ] else [ ];

    # Cloudflare
    services.reverseProxies.services.ai.provider = "cloudflare";
    services.reverseProxies.services.ai.oci-container.name = "local-ai";
    services.reverseProxies.services.ai.oci-container.port = 8080;

    # Dashboard
    services.homer.categories.Autres.services.LocalAI = {
      subtitle = "Managing documents";
      url = "https://ai.nicolasguilloux.eu";
      logo = "https://github.com/go-skynet/LocalAI/assets/2420543/0966aa2a-166e-4f99-a3e5-6c915fc997dd";
    };
  };
}
