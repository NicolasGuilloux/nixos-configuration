{ config, pkgs, ... }:

let
  cfg = config.services.uptime-kuma;
in
{
  services.uptime-kuma.enable = true;
  services.uptime-kuma.package = pkgs.unstable.uptime-kuma;

  # Reverse proxy
  services.reverseProxies.services.monitor.port = cfg.port;
  services.reverseProxies.services.monitor.provider = "cloudflare";
}
