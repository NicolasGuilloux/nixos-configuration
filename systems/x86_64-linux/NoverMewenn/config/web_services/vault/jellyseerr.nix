{ ... }:

{
  virtualisation.oci-containers.containers.jellyseerr = {
    image = "fallenbagel/jellyseerr";
    volumes = [ "/var/lib/jellyseerr/docker:/app/config" ];
    extraOptions = [ "--network=host" ];
    environment = {
      TZ = "Europe/Paris";
    };
  };

  services.reverseProxies.services.jellyseerr.oci-container.name = "jellyseerr";
  services.reverseProxies.services.jellyseerr.oci-container.port = 5055;
  services.reverseProxies.services.jellyseerr.provider = "cloudflare";
  services.reverseProxies.services.jellyseerr.sso.enable = false;

  # Dashboard
  services.homer.categories.Multimedia.services.Jellyseerr = {
    subtitle = "Requesting media";
    url = "https://jellyseerr.nicolasguilloux.eu";
    logo = "https://www.myqnap.org/wp-content/uploads/jellyseerr-logo.png";
  };
}
