{ pkgs, ... }:

{
  services.jellyfin.enable = true;
  services.jellyfin.openFirewall = true;
  services.jellyfin.package = pkgs.unstable.jellyfin;

  services.reverseProxies.services.media.port = 8096;
  services.reverseProxies.services.media.provider = "cloudflare";
  services.reverseProxies.services.media.sso.enable = false;

  # Dashboard
  services.homer.categories.Multimedia.services.Jellyfin = {
    subtitle = "Streaming media";
    url = "https://media.nicolasguilloux.eu";
    logo = "https://image.apktoy.com/img/4b/org.jellyfin.mobile/icon.png";
  };

  environment.systemPackages = [
    pkgs.jellyfin
    pkgs.jellyfin-web
    pkgs.jellyfin-ffmpeg
  ];
}
