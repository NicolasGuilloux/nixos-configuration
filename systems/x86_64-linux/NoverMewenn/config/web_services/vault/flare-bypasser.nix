{ ... }:

{
  # FlareSolverr
  virtualisation.oci-containers.containers.flare-bypasser = {
    image = "ghcr.io/yoori/flare-bypasser:main";
    ports = [ "8192:8080" ];
    volumes = [ "/var/lib/flare-bypasser/var:/opt/flare_bypasser/var" ];
  };
}
