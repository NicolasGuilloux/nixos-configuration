{ pkgs, ... }:

{
  services.prowlarr.enable = true;
  services.prowlarr.openFirewall = true;
  services.prowlarr.package = pkgs.unstable.prowlarr;

  services.reverseProxies.services.prowlarr = {
    port = 9696;
    provider = "cloudflare";
    sso.excludePathPrefixes = [ "/api" "/0/api" ];
  };

  # Dashboard
  services.homer.categories.Multimedia.services.Prowlarr = {
    subtitle = "Creating API from websites";
    url = "https://prowlarr.nicolasguilloux.eu";
    logo = "https://avatars.githubusercontent.com/u/73049443?s=200&v=4";
  };
}
