{ ... }:

{
  services.audible-downloader = {
    enable = true;
    group = "media";
    exportDir = "/mount/Multimedia/Livres Audio";
  };
}
