{ pkgs, ... }:

{
  services.plex.enable = true;
  services.plex.package = pkgs.unstable.plex;
  services.plex.openFirewall = true;

  systemd.services.plex.serviceConfig.CPUQuota = "600%";

  # Add plex to media group
  users.groups.media.members = [ "plex" ];

  services.reverseProxies.services.plex.port = 32400;
  services.reverseProxies.services.plex.provider = "cloudflare";
  services.reverseProxies.services.plex.sso.enable = false;
}
