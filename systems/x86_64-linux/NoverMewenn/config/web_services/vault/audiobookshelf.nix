{ config, pkgs, ... }:

{
  services.audiobookshelf.enable = true;
  services.audiobookshelf.package = pkgs.unstable.audiobookshelf;

  services.reverseProxies.services.book.port = config.services.audiobookshelf.port;
  services.reverseProxies.services.book.provider = "cloudflare";
  services.reverseProxies.services.book.sso.enable = false;

  # Add user to media group
  users.groups.media.members = [ config.services.audiobookshelf.user ];

  # Dashboard
  services.homer.categories.Multimedia.services."Audiobook shelf" = {
    subtitle = "Books and audiobooks management";
    url = "https://book.nicolasguilloux.eu";
    logo = "https://blog.mei-home.net/posts/audiobookshelf/logo.png";
  };
}
