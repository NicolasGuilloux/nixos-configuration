{ config, pkgs, ... }:

{
  # Service
  services.kavita.enable = true;
  services.kavita.package = pkgs.unstable.kavita;
  services.kavita.tokenKeyFile = config.age.secrets.kavita-token.path;

  # Agenix
  age.secrets.kavita-token.owner = config.services.kavita.user;

  # Reverse proxy
  services.reverseProxies.services.books = {
    port = config.services.kavita.settings.Port;
    provider = "cloudflare";
  };

  # Dashboard
  services.homer.categories.Multimedia.services.Kavita = {
    subtitle = "Managing Books";
    url = "https://books.nicolasguilloux.eu";
    logo = "https://avatars.githubusercontent.com/u/75760308?s=200&v=4";
  };
}
