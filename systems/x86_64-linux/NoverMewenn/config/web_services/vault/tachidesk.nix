{ ... }:

let
  port = 4567;
  extensions = [
    "https://github.com/keiyoushi/extensions"
  ];
in
{
  virtualisation.oci-containers.containers.tachidesk = {
    image = "ghcr.io/suwayomi/tachidesk";
    ports = [ "${toString port}:4567" ];
    volumes = [ "/var/lib/tachidesk:/home/suwayomi/.local/share/Tachidesk" ];
    environment = {
      TZ = "Europe/Paris";
      AUTO_DOWNLOAD_CHAPTERS = "true";
      EXTENSION_REPOS = "[\"${builtins.concatStringsSep "\", \"" extensions}\"]";
      FLARESOLVERR_ENABLED = "true";
    };
  };

  # Open Firewall
  networking.firewall.allowedTCPPorts = [ port ];

  # Dashboard
  services.homer.categories.Multimedia.services.TachiDesk = {
    subtitle = "Managing manga library";
    url = "https://tachidesk.nicolasguilloux.eu";
    logo = "https://i0.wp.com/community.refold.la/wp-content/uploads/2022/05/faviconlogo1.png?fit=3922%2C3922&ssl=1";
  };

  # Reverse proxy
  services.reverseProxies.services.tachidesk = {
    port = port;
    provider = "cloudflare";
  };
}
