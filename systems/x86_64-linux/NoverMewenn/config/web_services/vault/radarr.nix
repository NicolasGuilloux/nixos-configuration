{ pkgs, ... }:

{
  services.radarr.enable = true;
  services.radarr.package = pkgs.unstable.radarr;
  services.radarr.dataDir = "/var/lib/radarr";
  services.radarr.group = "media";
  services.radarr.openFirewall = true;

  services.reverseProxies.services.radarr = {
    port = 7878;
    provider = "cloudflare";
    sso.excludePathPrefixes = [ "/api" ];
  };

  # Dashboard
  services.homer.categories.Multimedia.services.Radarr = {
    subtitle = "Fetching movies";
    url = "https://radarr.nicolasguilloux.eu";
    logo = "https://avatars.githubusercontent.com/u/25025331?s=200&v=4";
  };
}
