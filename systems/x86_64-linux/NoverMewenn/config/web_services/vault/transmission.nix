{ config, lib, ... }:

let
  dataDir = "/var/lib/transmission";
  port = 9091;

  transmissionEnvTransformer = lib.mapAttrs' (
    name: value: {
      name = "TRANSMISSION_" + lib.toUpper (lib.stringAsChars (x: if x == "-" then "_" else x) name);
      value = toString value;
    }
  );
in
{
  virtualisation.oci-containers.containers.transmission = {
    autoStart = true;
    image = "haugene/transmission-openvpn";
    ports = [ "${toString port}:9091" ];
    extraOptions = [ "--privileged" ];
    volumes = [
      "${dataDir}:${dataDir}"
      "${dataDir}/.config:/config"
    ];
    environmentFiles = [ config.age.secrets.protonvpn.path ];
    environment = {
      TZ = config.time.timeZone;
      PGID = toString config.users.groups.media.gid;
      TRANSMISSION_WEB_UI = "flood-for-transmission";
      OPENVPN_PROVIDER = "PROTONVPN";
      OPENVPN_CONFIG = "nl.protonvpn.tcp";
      LOCAL_NETWORK = "192.168.0.0/16";
    } // transmissionEnvTransformer {
      # Data directories
      download-dir = dataDir;
      incomplete-dir = "${dataDir}/.incomplete";

      # Alternative speed limits
      alt-speed-enabled = "true";
      alt-speed-down = 50;
      alt-speed-up = 50;
      alt-speed-time-enabled = "true";
      alt-speed-time-begin = 420;
      alt-speed-time-end = 120;
    };
  };

  # Reverse proxy
  services.reverseProxies.services.download = {
    port = port;
    provider = "cloudflare";
  };

  # Dashboard
  services.homer.categories.Multimedia.services.Transmission = {
    subtitle = "Downloading torrent";
    url = "https://download.nicolasguilloux.eu";
    logo = "https://avatars.githubusercontent.com/u/223312?s=200&v=4";
  };
}
