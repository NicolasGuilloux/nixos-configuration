{ pkgs, ... }:

{
  services.sonarr.enable = true;
  services.sonarr.package = pkgs.unstable.sonarr;
  services.sonarr.dataDir = "/var/lib/sonarr";
  services.sonarr.group = "media";
  services.sonarr.openFirewall = true;

  services.reverseProxies.services.sonarr = {
    port = 8989;
    provider = "cloudflare";
    sso.excludePathPrefixes = [ "/api" ];
  };

  # Dashboard
  services.homer.categories.Multimedia.services.Sonarr = {
    subtitle = "Fetching series";
    url = "https://sonarr.nicolasguilloux.eu";
    logo = "https://res.cloudinary.com/razordarkamg/image/upload/v1621212884/SonarrV3_pufacd.png";
  };
}
