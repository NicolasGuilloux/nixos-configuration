{ config, lib, ... }:

let
  cfg = {
    port = 11434;
    webUiPort = 3000;
    openFirewall = true;
    dataDir = "/var/lib/ollama";
  };
in
{
  config = lib.mkIf config.device.ai {
    virtualisation.oci-containers.containers.ollama = {
      image = "ollama/ollama:0.3.7-rc5";
      extraOptions = [ "--gpus" "all" ];
      volumes = [ "${cfg.dataDir}/ollama:/root/.ollama" ];
      ports = [ "${toString cfg.port}:11434" ];
    };

    virtualisation.oci-containers.containers.ollama-webui = {
      image = "ghcr.io/open-webui/open-webui:main";
      extraOptions = [ "--add-host" "host.docker.internal:host-gateway" ];
      ports = [ "${toString cfg.webUiPort}:8080" ];
      volumes = [ "${cfg.dataDir}/webui:/app/backend/data" ];
      environment.OLLAMA_BASE_URL = "http://host.docker.internal:${toString cfg.port}";
    };

    # Firewall
    networking.firewall.allowedTCPPorts = if cfg.openFirewall then [ cfg.port cfg.webUiPort ] else [ ];

    # Cloudflare
    # services.reverseProxies.services.ai.provider = "cloudflare";
    # services.reverseProxies.services.ai.oci-container.name = "local-ai";
    # services.reverseProxies.services.ai.oci-container.port = 8080;

    # Dashboard
    services.homer.categories.Autres.services.Ollama = {
      subtitle = "Managing documents";
      url = "http://192.168.2.4:${toString cfg.webUiPort}";
      logo = "https://ollama.com/public/ollama.png";
    };
  };
}
