{ config, lib, ... }:

{
  virtualisation.oci-containers.containers.fgc = {
    image = "ghcr.io/vogler/free-games-claimer";
    # ports = [ "6080:6080" ];
    volumes = [ "/var/lib/fgc:/fgc/data" ];
    environmentFiles = [ config.age.secrets.fgc-env.path ];
  };

  # Restarts only on failure
  systemd.services.podman-fgc.serviceConfig.Restart = lib.mkForce "on-failure";

  # Cloudflare
  services.reverseProxies.services.fgc.provider = "cloudflare";
  services.reverseProxies.services.fgc.oci-container.name = "fgc";
  services.reverseProxies.services.fgc.oci-container.port = 6080;

  # Start it every night at 4am
  services.cron = {
    enable = true;
    systemCronJobs = [ "0 4 * * *     root     systemctl restart podman-fgc.service" ];
  };
}
