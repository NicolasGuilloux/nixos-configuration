{ config, ... }:

{
  # The redirection part is managed directly from the Cloudflare backoffice
  # Declare DNS entry
  systemd.services."cloudflared-boardgames-dns-record" = {
    enable = true;
    description = "Create the DNS entry \"boardgames\" to point to this server";
    wantedBy = [ "cloudflared-tunnel-${config.networking.hostName}.service" ];
    script = ''
      ${config.services.cloudflared.package}/bin/cloudflared tunnel route dns ${config.networking.hostName} boardgames || exit 0 
    '';
    serviceConfig = {
      User = "cloudflared";
    };
  };
}
