{ pkgs, ... }:

let
  cfg = {
    port = 8465;
    openFirewall = true;
    dataDir = "/var/lib/ownfoil";
    config = {
      library.paths = [ "/games" ];
      shop = {
        encrypt = false;
        motd = "Bienvenue dans le magasin de jeux de Nover.";
        public = true;
      };

      titles = {
        language = "fr";
        region = "FR";
        valid_keys = true;
      };
    };
  };
in
{
  virtualisation.oci-containers.containers.ownfoil = {
    image = "a1ex4/ownfoil";
    # ports = [ "${toString cfg.port}:8465" ];
    volumes = [
      "${cfg.dataDir}/config:/app/config"
      "${cfg.dataDir}/games:/games"
      "${pkgs.writeText "settings.yaml" (builtins.toJSON cfg.config)}:/app/config/settings.yaml"
    ];
  };

  # Firewall
  # networking.firewall.allowedTCPPorts = if cfg.openFirewall then [ cfg.port ] else [ ];

  # Reverse proxy
  services.reverseProxies.services.ownfoil.provider = "cloudflare";
  services.reverseProxies.services.ownfoil.oci-container.name = "ownfoil";
  services.reverseProxies.services.ownfoil.oci-container.port = 8465;
  services.reverseProxies.services.ownfoil.sso.enable = false;
}
