{ ... }:

{
  virtualisation.oci-containers.containers.simple-http-server = {
    image = "jdkelley/simple-http-server:latest";
    volumes = [ "/var/lib/files:/serve" ];
  };

  services.reverseProxies.services.files = {
    oci-container.name = "simple-http-server";
    oci-container.port = 8000;
    provider = "cloudflare";
    sso.enable = true;
    sso.excludePathPrefixes = [
      "/YouTube.ipa"
      "/uYouPlus.ipa"
      "/apps.json"
    ];
  };
}
