{ ... }:

{
  virtualisation.oci-containers.containers.music-assistant = {
    environment.TZ = "Europe/Paris";
    image = "ghcr.io/music-assistant/server:beta";
    extraOptions = [ "--network=host" "--privileged" ];
    volumes = [
      "/var/lib/music-assistant:/data"
      "/etc/localtime:/etc/localtime:ro"
    ];
  };

  # Open Firewall
  networking.firewall.allowedTCPPorts = [ 8095 8097 8098 ];

  # Dashboard
  services.homer.categories.Domotique.services."Music Assistant" = {
    subtitle = "Music aggregator for all providers";
    url = "http://192.168.2.4:8095";
    logo = "https://avatars.githubusercontent.com/u/71128003?s=200&v=4";
    tag = "local";
  };
}
