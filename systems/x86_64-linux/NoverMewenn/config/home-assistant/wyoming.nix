{ ... }:

{
  # virtualisation.oci-containers.containers.wyoming-piper = {
  #   image = "rhasspy/wyoming-piper";
  #   ports = [ "10200:10200" ];
  #   cmd = [ "--debug" "--voice" "fr-siwis-medium" ];
  # };

  # virtualisation.oci-containers.containers.wyoming-whisper = {
  #   image = "rhasspy/wyoming-whisper";
  #   ports = [ "10300:10300" ];
  #   cmd = [ "--language" "fr" "--model" "medium-int8" "--beam" "5" ];
  # };

  # virtualisation.oci-containers.containers.wyoming-openwakeword = {
  #   image = "rhasspy/wyoming-openwakeword";
  #   ports = [ "10400:10400" ];
  #   volumes = [ "/var/lib/wyoming/openwakeword:/custom" ];
  #   cmd = [ "--preload-model" "ok_nabu" "--custom-model-dir" "/custom" ];
  # };

  # # Set timeout when service turns off
  # systemd.services.podman-wyoming-piper.serviceConfig.TimeoutStopSec = lib.mkForce 10;
  # systemd.services.podman-wyoming-whisper.serviceConfig.TimeoutStopSec = lib.mkForce 10;
  # systemd.services.podman-wyoming-openwakeword.serviceConfig.TimeoutStopSec = lib.mkForce 10;

  # # Open Firewall
  # networking.firewall.allowedTCPPorts = [
  #   10200
  #   10300
  #   10400
  # ];
}
