{ pkgs, ... }:

{
  systemd.services.parcel-email-parser = {
    enable = true;
    description = "Parcel email parser to Home Assistant.";
    after = [ "network.target" ];
    wantedBy = [ "multi-user.target" ];

    script = ''
      ${pkgs.php83}/bin/php ./bin/console app:mailbox:process
      ${pkgs.php83}/bin/php ./bin/console app:parcels:outdated
    '';

    serviceConfig = {
      WorkingDirectory = "/home/nover/Projets/ParcelEmailParser";
      User = "nover";
      Group = "users";
      Restart = "always";
      RestartSec = "60s";
    };
  };
}
