{ ... }:

{
  services.home-assistant-remote = {
    enable = true;
    virtualMachines = [ "windows-11-gpu" "Carrefour" ];
    authorizedSshKeys = [
      "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAILiqJuYwVdWIDOktwOLXgAReeKyco+NKZSLmIetWCmig HomeAssistant"
    ];
  };
}
