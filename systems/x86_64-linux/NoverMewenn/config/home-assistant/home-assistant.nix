{ ... }:

{
  virtualisation.oci-containers.containers.home-assistant = {
    environment.TZ = "Europe/Paris";
    image = "ghcr.io/home-assistant/home-assistant";
    extraOptions = [ "--network=host" ];
    volumes = [
      "/var/lib/home-assistant:/config"
      "/etc/localtime:/etc/localtime:ro"
      "/var/run/dbus:/run/dbus:ro"
    ];
  };

  # Open Firewall
  networking.firewall.allowedTCPPorts = [
    8123
    21064 # Homekit Bridge
    1400 # Sonos
  ];

  # Cloudflare
  services.reverseProxies.services.home.port = 8123;
  services.reverseProxies.services.home.provider = "cloudflare";
  services.reverseProxies.services.home.sso.enable = false;

  # Dashboard
  services.homer.categories.Domotique.services.HomeAssistant = {
    subtitle = "Home automation";
    url = "https://home.nicolasguilloux.eu";
    logo = "https://upload.wikimedia.org/wikipedia/commons/thumb/6/6e/Home_Assistant_Logo.svg/1038px-Home_Assistant_Logo.svg.png?20170316121847";
  };
}
