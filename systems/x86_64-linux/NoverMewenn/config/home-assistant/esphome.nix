{ ... }:

{
  virtualisation.oci-containers.containers.esphome = {
    environment.TZ = "Europe/Paris";
    image = "ghcr.io/esphome/esphome";
    extraOptions = [ "--network=host" "--privileged" ];
    volumes = [
      "/var/lib/esphome:/config"
      "/etc/localtime:/etc/localtime:ro"
    ];
  };

  # Open Firewall
  networking.firewall.allowedTCPPorts = [ 6052 ];

  # Dashboard
  services.homer.categories.Domotique.services.ESPHome = {
    subtitle = "Dashboard for managing ESP devices on the network";
    url = "https://esphome.nicolasguilloux.eu/";
    logo = "https://avatars.githubusercontent.com/u/45919759?s=200&v=4";
  };

  # Reverse proxy
  services.reverseProxies.services.esphome = {
    port = 6052;
    provider = "cloudflare";
  };
}
