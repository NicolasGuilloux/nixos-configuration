{ ... }:

{
  services.mosquitto.enable = true;
  services.mosquitto.listeners = [
    {
      users.mqtt.hashedPassword = "$7$101$cY+pZdopkGI43TdJ$Q+mv6YmNZRzur0aUGpNNBVy9sigPC4rdiWzNPCR1wWjF663w6MUolyKRCqMpMQVwf96fC7aWfho4xwZ99VjOpA==";
      acl = [
        "pattern readwrite #"
        "topic readwrite #"
      ];
    }
  ];

  networking.firewall.allowedTCPPorts = [ 1883 ];
}
