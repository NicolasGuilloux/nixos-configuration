{ ... }:

let port = 10444;
in
{
  virtualisation.oci-containers.containers.scrypted = {
    autoStart = true;
    image = "koush/scrypted";
    extraOptions = [ "--network=host" ];
    volumes = [ "/var/lib/scrypted:/server/volume" ];
    environment = {
      SCRYPTED_INSECURE_PORT = toString port;
    };
  };

  networking.firewall.allowedTCPPorts = [
    port # UI
    32606 # HomeKit
  ];

  # Dashboard
  services.homer.categories.Domotique.services.Scrypted = {
    subtitle = "Managaging Cameras";
    url = "http://192.168.2.4:${toString port}/#/";
    logo = "https://docs.scrypted.app/img/logo.png";
    tag = "local";
  };
}
