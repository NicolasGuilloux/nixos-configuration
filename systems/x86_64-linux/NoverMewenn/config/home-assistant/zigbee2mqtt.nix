{ config, ... }:

{
  services.zigbee2mqtt.enable = true;
  services.zigbee2mqtt.settings = {
    homeassistant = true;
    serial.port = "/dev/serial/by-id/usb-ITead_Sonoff_Zigbee_3.0_USB_Dongle_Plus_688ee8d155beed11a8eb6b2e38a92db5-if00-port0";
    frontend.port = 8084;
    advanced.channel = 25;
    availability = true;

    mqtt = {
      base_topic = "zigbee2mqtt";
      server = "mqtt://localhost:1883";
      client_id = "Zigbee2MQTT";
      user = "mqtt";
      password = "!secret.yaml mqtt_password";
    };
  };

  # Open Firewall
  networking.firewall.allowedTCPPorts = [ config.services.zigbee2mqtt.settings.frontend.port ];

  # Dashboard
  services.homer.categories.Domotique.services.Zigbee2MQTT = {
    subtitle = "Managing Zigbee network";
    url = "http://192.168.2.4:${toString config.services.zigbee2mqtt.settings.frontend.port}/#/";
    logo = "https://github.com/Koenkk/zigbee2mqtt/raw/master/images/logo.png";
    tag = "local";
  };
}
