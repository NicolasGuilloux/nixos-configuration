{ inputs, config, lib, ... }:

let
  nixvirtLib = inputs.nixvirt.lib;

  baseWindowsVmConfig = nixvirtLib.domain.templates.windows {
    name = "windows-11-gpu";
    uuid = "cc7439ed-36af-4696-a6f2-1f0c4474d87e";
    memory = { count = 16; unit = "GiB"; };
    storage_vol = "/home/nover/Documents/Windows VM/windows-11.qcow2";
    nvram_path = "/etc/ovmf/edk2-i386-vars.fd";
    virtio_net = true;
  };

  mergedWindowsVmConfig = lib.nover.recursiveMerge [
    baseWindowsVmConfig
    {
      title = "Windows 11 GPU";
      vcpu.count = 8;
      os.smbios.mode = "host";
      features.kvm.hidden.state = true;
      devices.interface.source.bridge = "br0";

      cpu = {
        migratable = true;
        topology = {
          sockets = 1;
          dies = 1;
          cores = 4;
          threads = 2;
        };
      };
    }
  ];

  windowsVmConfig = mergedWindowsVmConfig // {
    devices = with mergedWindowsVmConfig.devices; {
      inherit emulator disk interface input sound;

      hostdev = [
        {
          mode = "subsystem";
          type = "pci";
          managed = true;
          source = {
            address = {
              domain = 0;
              bus = 7;
              slot = 0;
              function = 0;
            };
          };
          address = {
            type = "pci";
            domain = 0;
            bus = 6;
            slot = 0;
            function = 0;
          };
        }
      ];
    };
  };
in
{
  boot.kernelModules = [ "vfio" "vfio_iommu_type1" "vfio_pci" ];

  virtualisation.libvirtd.virtualMachines.windows-11-gpu = {
    unbindVTconsoles = false;
    unbindEfiFramebuffer = false;
    raceConditionTimeout = 5;
    detachIommuGroups = [ 16 ];
    kernelModules.unload = [ "nvidia_drm" "nvidia_uvm" "nvidia_modeset" "nvidia" ];
    services.stop = [ ]
      ++
      (if config.hardware.nvidia.nvidiaPersistenced then
        [ "nvidia-persistenced.service" ]
      else
        [ ]);
    cpuAllocation.enable = true;
    cpuAllocation.totalThreadsCount = 16;
    cpuAllocation.threadsAllocated = 8;
  };

  # VM declaration
  virtualisation.libvirt.enable = false;
  virtualisation.libvirt.swtpm.enable = true;
  virtualisation.libvirt.connections."qemu:///session".domains = [
    { definition = nixvirtLib.domain.writeXML windowsVmConfig; }
    { definition = lib.snowfall.fs.get-file "assets/libvirt/windows-11-nogpu.xml"; }
    # { definition = lib.snowfall.fs.get-file "assets/libvirt/windows-11-gpu.xml"; }
  ];
}
