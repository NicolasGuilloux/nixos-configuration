{ lib, ... }:

{
  virtualisation.libvirt.connections."qemu:///session".domains = [
    {
      definition = lib.nover.write.linux {
        name = "Carrefour";
        uuid = "2150f7c5-a614-4242-a803-c15065d828e5";
        memory = { count = 4; unit = "GiB"; };
        storage_vol = "/home/nover/Documents/Carrefour.qcow2";
        virtio_video = false;
      };
    }
  ];

  # Firewall
  networking.firewall.allowedTCPPorts = [ 5900 5901 ];
}
