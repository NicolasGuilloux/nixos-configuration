{ config, pkgs, lib, ... }:

{
  # Enable OpenGL
  hardware.opengl = {
    # Enables the graphics driver for OpenGL
    enable = lib.mkDefault true;
    # Enables Direct Rendering Infrastructure (DRI), which allows the graphics driver to directly render graphics, improving performance in OpenGL
    driSupport = lib.mkDefault true;
    # Enables 32-bit Direct Rendering Infrastructure (DRI) support, which allows the graphics driver to directly render graphics in 32-bit applications using OpenGL
    driSupport32Bit = lib.mkDefault true;
    # Adds the 'vaapiVdpau' package to the extra packages for OpenGL
    extraPackages = with pkgs; [ vaapiVdpau ];
  };

  # Nouveau drivers
  # boot.kernelModules = [ "nouveau" ];
  # boot.blacklistedKernelModules = [ "nvidia" "nvidia_uvm" "nvidia_drm" "nvidia_modeset" ];
  # services.xserver.videoDrivers = [ "nouveau" ];

  # Nvidia drivers
  boot.blacklistedKernelModules = [ "nouveau" ];
  services.xserver.videoDrivers = [ "nvidia" ];
  hardware.nvidia = {
    modesetting.enable = true;
    nvidiaSettings = true;
    package = # pkgs.nvidia-patch.patch-nvenc (
      #pkgs.nvidia-patch.patch-fbc 
      config.boot.kernelPackages.nvidiaPackages.vulkan_beta
      #)
    ;

    # Power management
    powerManagement.enable = false;
    powerManagement.finegrained = false;

    # Headless mode
    # nvidiaPersistenced = true;

    # Opensource Drivers
    # open = true;
  };

  # Top GPU program
  programs.atop.atopgpu.enable = true;

  # Apparently this is needed to load nvidia drivers

  # Enable various nvidia stuff
  environment.systemPackages = with pkgs; [ nvtopPackages.nvidia cudatoolkit ];

  # Enable Nvidia for Podman
  # virtualisation.podman.enableNvidia = true;
  hardware.nvidia-container-toolkit.enable = true;
}
