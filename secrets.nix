# https://nixos.wiki/wiki/Agenix

let
  mapAttrs' = f: set:
    builtins.listToAttrs
      (map (attr: f attr set.${attr})
        (builtins.attrNames set));

  mewennKeys = [
    # nover user
    "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIG7lrPthNbMa6mmqiDPCZbOZZXudGX+vBFDPYaB8ugf+ nicolas.guilloux@proton.me"
    # host
    "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIJXh2x7ZwGHI/2yC17rRG46RemKFm2Fa+0snZ/dGlNz2 root@NoverMewenn"
    "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQDL2+V+4b0kHnzrxq1uzhJjTYMEzDDCyvO+5oq10PJ1V3TCK/zfqMjvGZKbJRb7dm6DqfGzypSkp33GQJejJjv5QkgcbYZ4nhlaQHFlBTccTgWIws6qTs6aJ+XlXcPa6Vggk+nGpku85cnOEKhqLoUNgNRWr2TkX1zTEgMwwfCT6tgo8qivMqQKPs1koVEG1kCxbkOgqd0MsjlBwgd13uZa/exgOFuj4Hh1wQehPT7uQmbRcJP3nTM6puoI9kgV30z6giUveZt/1zHTnOoyOmi2IRBuPJuXUT7hdjktCY0VcJWggxyoq6n+qcNx57qAGnaiso11Afn4/n2deyfvaiP1mJKbeia+OnPBxofvudHrwxw7wMjYVZWxDBC4QNn5SQkJoxovVon2yHdgn7CFgeTEz8rOhbwVaoKlz5EQB3mPgF2wW8rhKCkJdFQbvLq3peGgkggUbX0ngpMjB69ale2zxQn0UOOeKlqF6RU461YnXNTkfL7AKEsSeXL1CSNDTFDAauHeYBVbA0fd9UsiQNG2abHwODjIpZ6ktgLwiE75bgCeK6RL/DIZec0AlpkB7GoXnO+T9WSM+PWagOC6/V22X1sV5gmtC2z6pVicxQq3i5qTBfkqyw+a5OeK2rvzO3kq5AsnIW08gJ0N2I1ITdu9/cSf5QUlydLRjkYQefHWkQ== root@NoverMewenn"
  ];
in

mapAttrs'
  (name: value: {
    name = "assets/secrets/NoverMewenn/${name}.age";
    value = {
      publicKeys = value;
    };
  })
{
  # Mewenn
  "audible" = mewennKeys;
  "authelia_jwt_secret" = mewennKeys;
  "authelia_session_secret" = mewennKeys;
  "authelia_storage_encryption_key" = mewennKeys;
  "authentik-env" = mewennKeys;
  "cert-authority" = mewennKeys;
  "cert-authority-key" = mewennKeys;
  "cloudflared-credentials" = mewennKeys;
  "courier-env" = mewennKeys;
  "crowdsec-enroll-key" = mewennKeys;
  "fgc-env" = mewennKeys;
  "fgc-alex-env" = mewennKeys;
  "gitlab-runner-env" = mewennKeys;
  "invidious-env" = mewennKeys;
  "kavita-token" = mewennKeys;
  "nix-serve-private-key" = mewennKeys;
  "paperless" = mewennKeys;
  "paperless-env" = mewennKeys;
  "pcloud" = mewennKeys;
  "protonvpn" = mewennKeys;
  "traefik-env" = mewennKeys;
  "vault" = mewennKeys;
  "authelia-oidc-issuer-private-key" = mewennKeys;
  "authelia-oidc-hmac-secret" = mewennKeys;
}
