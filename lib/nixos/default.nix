{ lib }:

with lib;

{
  /* Creates a sets of attributes from a list

    Example:
    mapToAttrs (value: { name = "name-${value}"; value = "value-${value}"; }) [ "a" "b"]
    => { name-a = "value-a"; name-b = "value-b"; }
  */
  mapToAttrs =
    # A function that returns { name = ...; value = ...; }
    f:
    # A list of string 
    list:
    lib.listToAttrs (map f list);

  recursiveMerge = attrList:
    let
      f = attrPath:
        zipAttrsWith (n: values:
          if tail values == [ ]
          then head values
          else if all isList values
          then unique (concatLists values)
          else if all isAttrs values
          then f (attrPath ++ [ n ]) values
          else last values
        );
    in
    f [ ] attrList;
}
