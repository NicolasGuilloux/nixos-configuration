{ lib, inputs, ... }:

with lib;

let
  templates = {
    base = import ./templates/base.nix { inherit lib inputs; };
    linux =
      # NixVirt config template
      templateConfig:
      # Extra qemu config
      extraConfig:
      import ./templates/linux.nix { inherit lib inputs templateConfig extraConfig; };
  };
in
{
  inherit templates;

  write.linux =
    # NixVirt config
    templateConfig:
    inputs.nixvirt.lib.domain.writeXML (templates.linux templateConfig { });
}
