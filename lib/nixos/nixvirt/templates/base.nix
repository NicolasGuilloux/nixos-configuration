{ ... }:

{
  # Set the bridged network interface
  devices.interface.source.bridge = "br0";

  # Open Spice graphics to local network
  graphics = {
    type = "spice";
    autoport = false;
    image.compression = false;
    listen = {
      type = "address";
      address = "0.0.0.0";
    };
  };
}
