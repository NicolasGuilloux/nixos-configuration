#!/usr/bin/env bash
# bash <(curl https://gitlab.com/NicolasGuilloux/nixos-configuration/-/raw/master/scripts/rescue.sh)

cd /etc/nixos
nix-env -iA nixos.git
git clone https://gitlab.com/NicolasGuilloux/nixos-configuration.git

