{ lib
, stdenv
, fuse
, zlib
, cmake
, boost
, autoPatchelfHook
, makeWrapper
, udev
, pkg-config
, unzip
, ...
}:

let
  boostPkg = boost.override { enableShared = false; enabledStatic = true; };
in
stdenv.mkDerivation rec {
  pname = "pcloudcc";
  version = "2.0.1";

  src = builtins.fetchurl {
    url = "https://github.com/pcloudcom/console-client/archive/refs/heads/master.zip";
    sha256 = "0j2ynn3cp7ypa3jsk7iapcgprk9x9k286p8yq48scksrln6if4lg";
  };

  dontUseCmakeConfigure = true;

  nativeBuildInputs = [
    cmake
    boostPkg
    pkg-config
    autoPatchelfHook
    makeWrapper
  ];

  buildInputs = [
    stdenv.cc.cc.lib
    pkg-config
    fuse
    cmake
    zlib
    udev
    boostPkg
    unzip
  ];

  installPhase = ''
    mkdir -p $out/{bin,share}
    cp -r ./pCloudCC/* $out/share

    # Build pclsync
    cd $out/share/lib/pclsync
    make clean
    make fs

    # Build mbedtls
    cd $out/share/lib/mbedtls
    cmake . -Wno-dev
    make clean
    make

    # Build Application
    cd $out/share
    cmake . -Wno-dev
    make clean
    make
    make DESTDIR=$out CMAKE_INSTALL_PREFIX=$out install

    # Move the compiled binaries and library
    mv $out/var/empty/local/lib $out/
    mv $out/var/empty/local/bin $out/
  '';
}
