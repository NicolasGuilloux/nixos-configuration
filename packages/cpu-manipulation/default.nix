{ stdenv
, writeShellScript
, systemd
, getconf
, ...
}:

stdenv.mkDerivation rec {
  pname = "cpu-manipulation";
  version = "1.0.0";

  src = writeShellScript "cpu-manipulation" ''
    action=$1
    cores_count=$2

    while [ $# -ne 0 ]
    do
        arg="$1"
        case "$arg" in
            -v)
              set -e 
              set -x
              ;;
        esac
        shift
    done
    
    total_cores_count=$(${getconf}/bin/getconf _NPROCESSORS_ONLN)

    if [ "$action" == "reserve" ]; then
      echo "Bind the host to the first $(expr $total_cores_count - $cores_count) cores"
      ${systemd}/bin/systemctl set-property --runtime -- user.slice AllowedCPUs=0-$(expr $total_cores_count - $cores_count - 1)
      ${systemd}/bin/systemctl set-property --runtime -- system.slice AllowedCPUs=0-$(expr $total_cores_count - $cores_count - 1)
      ${systemd}/bin/systemctl set-property --runtime -- init.scope AllowedCPUs=0-$(expr $total_cores_count - $cores_count - 1)

    elif [ "$action" == "free" ]; then
        echo "Bind the host to all the cores"
        ${systemd}/bin/systemctl set-property --runtime -- user.slice AllowedCPUs=0-$total_cores_count
        ${systemd}/bin/systemctl set-property --runtime -- system.slice AllowedCPUs=0-$total_cores_count
        ${systemd}/bin/systemctl set-property --runtime -- init.scope AllowedCPUs=0-$total_cores_count

    else
        echo "Unsupported action $1. Use reserve or free action.";
        exit 1;
    fi
  '';

  dontUnpack = true;

  installPhase = ''
    mkdir -p $out/bin
    cp $src $out/bin/cpu-manipulation
    chmod +x $out/bin/cpu-manipulation
  '';
}
